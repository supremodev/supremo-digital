'use strict';

var examples = {
  'supremo' : {
    list: (function generateLoveList() {
      var list = ['12 Supremo'];
      var nums = [5, 4, 3, 2, 2];
      var words = ('São Paulo,Rio de Janeiro,Salvador,Brasília,Fortaleza,Belo Horizonte,Manaus,Curitiba,Recife,Porto Alegre,Belém,Goiânia,Guarulhos,Campinas,São Luís,São Gonçalo,Maceió,Duque de Caxias,Natal,Campo Grande,Teresina,São Bernardo do Campo,Nova Iguaçu,João Pessoa,Santo André,Osasco,São José dos Campos,Jaboatão dos Guararapes,Ribeirão Preto,Uberlândia,Contagem,Sorocaba,Aracaju,Feira de Santana,Cuiabá,Joinville,Juiz de Fora,Londrina,Aparecida de Goiânia,Ananindeua,Niterói,Porto Velho,Campos dos Goytacazes,Belford Roxo,Juazeiro do Norte,Foz do Iguaçu,Sumaré,Volta Redonda,Barueri,Embu das Artes,Marabá,Ipatinga,Imperatriz,Viamão,Novo Hamburgo,São Carlos,Parnamirim,Magé,Marília,Sete Lagoas,Colombo,Macaé,Arapiraca,Divinópolis,São José,Itaboraí,São Leopoldo,Americana,Indaiatuba,Cotia,Jacareí,Araraquara,Presidente Prudente,Itapevi,Maracanaú,Itabuna,Juazeiro,Santa Luzia,Hortolândia,Rondonópolis,Dourados,Rio Grande,Cachoeiro de Itapemirim,Alvorada,Criciúma,Cabo Frio,Rio Verde,Chapecó,Itajaí,Sobral,Rio Claro,Cabo de Santo Agostinho,Passo Fundo,Araçatuba,Luziânia,Santa Bárbara d’Oeste,Lauro de Freitas,Castanhal,Angra dos Reis,Nova Friburgo,Parauapebas,Ferraz de Vasconcelos,Águas Lindas de Goiás,Ilhéus,Barra Mansa,Guarapuava,Nossa Senhora do Socorro,São José de Ribamar,Ibirité,Teresópolis,Mesquita,Araguaína,Francisco Morato,Itu,Itapecerica da Serra,Timon,Poços de Caldas,Jequié,Linhares,Caxias,Jaraguá do Sul,Pindamonhangaba,Bragança Paulista,Lages,Nilópolis,São Caetano do Sul,Teixeira de Freitas,Itapetininga,Palhoça,Alagoinhas,Camaragibe,Barreiras,Valparaíso de Goiás,Paranaguá,Parnaíba,Abaetetuba,Patos de Minas,Mogi Guaçu,Franco da Rocha,Porto Seguro,Maricá,Queimados,Pouso Alegre,Jaú').split(',');

      nums.forEach(function(n) {
        words.forEach(function(w) {
          list.push(n + ' ' + w);
        });
      });

      return list.join('\n');
    })(),
    option: '{\n' +
      '  gridSize: Math.round(16 * $(\'#canvas\').width() / 1024),\n' +
      '  weightFactor: function (size) {\n' +
      '    return Math.pow(size, 2) * $(\'#canvas\').width() / 1920;\n' +
      '  },\n' +
      '  fontFamily: \'Times, serif\',\n' +
      '  color: function (word, weight) {\n' +
      '    return (weight === 12) ? \'#025959\' : \'#333333\';\n' +
      '  },\n' +
      '  rotateRatio: 0.8,\n' +
      '  rotationSteps: 3,\n' +
      '}'
  }
};


jQuery(function($) {  
  var $canvas = $('#canvas');
  var $htmlCanvas = $('#html-canvas');
  var $loading = $('#loading');
  var $list = $('#input-list');
  var $options = $('#config-option');
  var $width = $('#config-width');
  var $height = $('#config-height');


  var run = function run() {    

    // Defina a largura e a altura
    var width = $width.val() ? $width.val() : $('#canvas-container').width();
    var height = $height.val() ? $height.val() : Math.floor(width * .3);
    var pixelWidth = width;
    var pixelHeight = height;
	  
    $canvas.attr('width', pixelWidth);
    $canvas.attr('height', pixelHeight);

   

    // Defina o objeto de opções
    var options = {};
    if ($options.val()) {
      options = (function evalOptions() {
        try {
          return eval('(' + $options.val() + ')');
        } catch (error) {
          alert('O seguinte erro de Javascript ocorreu na definição da opção; todas as opções serão ignoradas: \n\n' +
            error.toString());
          return {};
        }
      })();
    }

    // Coloque a lista de palavras em opções
    if ($list.val()) {
      var list = [];
      $.each($list.val().split('\n'), function each(i, line) {
        if (!$.trim(line))
	  	return;

        var lineArr = line.split(' ');
        var count = parseFloat(lineArr.shift()) || 0;
        list.push([lineArr.join(' '), count]);
      });
      options.list = list;
    }

    // Sempre limpe manualmente a saída html
    if (!options.clearCanvas) {
      $htmlCanvas.empty();
      //$htmlCanvas.css('background-color', options.backgroundColor || '#fff');
    }
    // Tudo pronto, chame o WordCloud()
    // O pedido é importante aqui, porque a tela HTML pode definido para exibir: nenhum
    WordCloud([$canvas[0], $htmlCanvas[0]], options);
  };

	
  var loadExampleData = function loadExampleData(name) {
    var example = examples[name];
    $options.val(example.option || '');
    $list.val(example.list || '');
    
  };

  var hashChanged = function hashChanged() {
    var name = window.location.hash.substr(1);
    if (!name) {
      // Se não houver nome, execute como está.
      run();
    } else if (name in examples) {
      // Se o nome corresponder a um exemplo, carregue-o e execute-o
      loadExampleData(name);
      run();
    } else {
      // Se o nome não corresponder, redefina-o.
      //window.location.replace('#');
    }
  }

  $(window).on('hashchange', hashChanged);

  if (!window.location.hash || !(window.location.hash.substr(1) in examples)) {
    // Se o hash inicial não corresponder a nenhum dos exemplos,
    // ou não existe, redefina-o para #supremo
    window.location.replace('#supremo');
  } else {
    hashChanged();
  }
});
