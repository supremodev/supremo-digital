
var url = document.location.origin;

//==================== NOVO DOCUMENTO =======================

$(".novo-ajax").click(function () {
    var vazio = $("input[name=titulo]").filter(function () { return !this.value; }).get();

    var destino = $(this).attr('destino');
    var evento = this;

    if (vazio.length) {

        $('#erro-atualizar').toggleClass('d-none');

    } else {

        $(evento).prop('disabled', true);
        $('#erro-atualizar').addClass('d-none');

        $.ajax({

            type: "POST",
            data: new FormData($('#formularios')[0]),
            cache: false,
            contentType: false,
            processData: false,
            url: url + "/weepy/dashboard/admin/" + destino,
            dataType: "html",
            success: function (retorno) {
                var msgModal = JSON.parse(retorno);
                if (msgModal) {
                    $('#atualizado-sucesso').removeClass('d-none');
                } else {
                    $('#erro-atualizar').toggleClass('d-none');
                }
            },
            beforeSend: function () {

            },
            complete: function (msg) {

            }
        });
    }

});

//==================== F =======================

$("#formularios input").keydown(function () {
    $('#atualizado-sucesso').addClass('d-none');
    $('#erro-atualizar').addClass('d-none');
    $('.novo-ajax').prop('disabled', false);
    $('.link-ajax-atualizar').prop('disabled', false);
});
$("#formularios textarea").keydown(function () {
    $('#atualizado-sucesso').addClass('d-none');
    $('#erro-atualizar').addClass('d-none');
    $('.novo-ajax').prop('disabled', false);
    $('.link-ajax-atualizar').prop('disabled', false);
});

$('.link-ajax-atualizar').on('click', function () {

    var evento = this;
    var destino = $(this).attr('destino');
    $(evento).prop('disabled', true);

    $.ajax({

        type: "POST",
        data: new FormData($('#formularios')[0]),
        cache: false,
        contentType: false,
        processData: false,
        url: url + "/weepy/dashboard/admin/" + destino,
        dataType: "html",
        success: function (retorno) {
            var msgModal = JSON.parse(retorno);
            if (msgModal) {
                $('#atualizado-sucesso').removeClass('d-none');
            } else {
                $('#erro-atualizar').toggleClass('d-none');
            }
        },
        beforeSend: function () {

        },
        complete: function (msg) {

        }
    });
});

$('.link-ajax').on('click', function () {

    var link = $(this).attr('link');
    $('.spinner').css({ display: "block" });

    $.ajax(url + "/weepy/dashboard/admin/paginas/" + link)
        .done(function (result) {
            $('#main').html(result);
        })
        .fail(function () {

        })
        .always(function () {
            $('.spinner').hide("slow");
        });
});

$("#atualizarPagina").click(function () {

    var fd = new FormData();
    fd.append('imagem', $('#inputImagem'));
    fd.append('titulo', $("input[name=nome_cliente]").val());

    $.ajax({

        type: "POST",
        data: fd,
        url: url + "/weepy/dashboard/admin/paginas/atualizar",
        dataType: "html",
        success: function (result) {
            alert('atualizado');
        },
        beforeSend: function () {

        },
        complete: function (msg) {

        }
    });
});

$("#atualizarPaginaa").click(function () {
    var evento = this;

    //$('.carregar-pagina').css({ display: "flex" });

    $('#atualizado-sucesso').toggleClass('d-none');

    $.ajax({
        url: url + "/weepy/dashboard/admin/paginas/atualizar",
        method: 'post',
        data: new FormData($('#myForm')[0]),
        cache: false,
        contentType: false,
        processData: false,
        success: function (retorno) {
            //$('.carregar-pagina').hide("slow");
            var msgModal = JSON.parse(retorno);
            if (msgModal) {
                $('#atualizado-sucesso').removeClass('d-none');
            } else {
                $('#erro-atualizar').toggleClass('d-none');
            }
        },
        beforeSend: function () {
            $(evento).prop('disabled', true);
        },
        complete: function (msg) {
            $(evento).prop('disabled', false);
        }
    });// Fim do ajax

});


// CONTATO JSON

$(".ajaxcontato").click(function () {
    var evento = this;

    var id = 1;

    $.ajax({
        url: url + "/weepy/dashboard/admin/contato/contatoJson",
        method: 'post',
        dataType: "html",
        data: {
            id: id,
        },
        success: function (retorno) {
            objeto = retorno;
            nome = JSON.stringify(retorno.nome);
            $('#teste').html(objeto);
            console.log(retorno);
            var msgModal = JSON.parse(retorno);
            console.log(msgModal);

        },
        beforeSend: function () {

        },
        complete: function (msg) {

        }
    });
});

// NOTIFICACAO JSON

var tid = setInterval(notificar, 8000);
function notificar() {

    var quantidadeNortificacao = $('#notificacao-ajax').attr('qtd');

    $.ajax({
        url: url + "/weepy/dashboard/admin/notificacao/verificar",
        method: 'post',
        dataType: "json",
        data: {
            qtd: quantidadeNortificacao,
        },
        success: function (retorno) {
            var msgModal = JSON.parse(retorno);
            if (msgModal) {
                ativarnotificacao();
            }
        },
        beforeSend: function () {

        },
        complete: function (msg) {

        }
    });
};
function ativarnotificacao() {

    var quantidadeNortificacao = $('#notificacao-ajax').attr('qtd');

    $.ajax({
        url: url + "/weepy/dashboard/admin/notificacao/certo",
        method: 'post',
        dataType: "html",
        data: {
            qtd: 1,
        },
        success: function (retorno) {
            $('#audio')[0].play();
            $('.count').text('1');
            $('.count').css('display', 'initial');
            $('#notificacao-ajax').html(retorno);
            $('#notificacao-ajax').attr('qtd', ++quantidadeNortificacao);
        },
        beforeSend: function () {

        },
        complete: function (msg) {

        }
    });
};

// CARREGAR FILA
function carregarFila() {

    $.ajax({
        url: url + "/weepy/dashboard/admin/reserva/carregarFila",
        method: 'post',
        dataType: "html",

        success: function (retorno) {
            $('.ajax-carregamento').html(retorno);
        },
        beforeSend: function () {
            $('.ajax-carregamento').addClass('carregar');
        },
        complete: function (msg) {
            $('.ajax-carregamento').removeClass('carregar');
        }
    });

}

// LISTA NOMES
function consultarnome() {
    var nome = $("input[name=nome-pesquisa]").val();

    $.ajax({
        url: url + "/weepy/dashboard/admin/cliente/consultaNome",
        method: 'post',
        dataType: "html",
        data: {
            nome: nome,
        },
        success: function (retorno) {
            $('#lista-nome').html(retorno);
            carregarFila();
        },
        beforeSend: function () {

        },
        complete: function (msg) {

        }
    });

}

$("#consultar-nome").click(function () {
    consultarnome();
});
$("input[name=nome-pesquisa]").bind("enterKey", function (e) {
    consultarnome();
});
$('input[name=nome-pesquisa]').keyup(function (e) {
    if (e.keyCode == 13) {
        $(this).trigger("enterKey");
    }
});


$("#lista-nome").click(function () {

    $nome = $('input[name=nomeselecionado]:checked').val();
    $id = $('input[name=nomeselecionado]:checked').attr('id');
    $("input[name=nome-pesquisa]").val($nome);
    $("input[name=id]").val($id);
    $(this).hide("slow");

});

// ATIVAR INPUT NOVO CLIENTE

var ativo = true;
$(".novo-cliente").click(function (e) {
    e.preventDefault();
    if (ativo) {
        $(".novo-input").show("slow");
        $(".pesquisa-input").hide();
        ativo = !ativo;
    }
    else {
        $(".pesquisa-input").show("slow");
        $(".novo-input").hide("slow");
        ativo = !ativo;
    }

});
// Registrar Reserva

$("#reserva").click(function () {

    var evento = this;

    $.ajax({
        url: url + "/weepy/dashboard/admin/reserva/analise",
        method: 'post',
        data: new FormData($('#formularios')[0]),
        cache: false,
        contentType: false,
        processData: false,
        dataType: "html",
        success: function (retorno) {
            var msgModal = JSON.parse(retorno);
            if (msgModal) {
                $('#atualizado-sucesso').removeClass('d-none');
            } else {
                $('#erro-atualizar').toggleClass('d-none');
            }
        },
        beforeSend: function () {
            $(evento).prop('disabled', true);
        },
        complete: function (msg) {
            $(evento).prop('disabled', false);
        }
    });// Fim do ajax

});
// FORMULARIO LOGIN

$("#botaoLogin").click(function () {
    var evento = this;



    $.ajax({
        url: url + "/weepy/dashboard/admin/login/acesso",
        method: 'post',
        data: new FormData($('#formLogin')[0]),
        cache: false,
        contentType: false,
        processData: false,
        success: function (retorno) {
            var msgModal = JSON.parse(retorno);
            console.log(msgModal);


        },
        beforeSend: function () {

        },
        complete: function (msg) {

        }
    });// Fim do ajax

});

$("#botaoLoginn").click(function () {

    var senha = $('input[name=senha]').val();
    var nome_email = $("input[name=nome_email]").val();

    $.ajax({

        type: "POST",
        data: {
            senha: senha,
            nome_email: email,
            grecaptcharesponse: grecaptcha.getResponse()
        },
        url: url + "/weepy/dashboard/admin/login/acesso",
        dataType: "html",
        success: function (result) {
            $('#teste').html(result);
            var msgModal = JSON.parse(result);
            console.log(msgModal);
            alert(msgModal);
            grecaptcha.reset();
        },
        beforeSend: function () {

        },
        complete: function (msg) {
            var msgModal = JSON.parse(msg);
            console.log(msgModal);
            alert(msgModal);
        }
    });
});


var contactForm = $("#formLoginn");
contactForm.on("submit", function (e) {

    e.preventDefault();

    var nome_email = $("input[name=nome_email]").val();
    var senha = $('input[name=senha]').val();

    $.ajax({
        type: "POST",
        url: url + "/weepy/dashboard/admin/login/teste", //Our file 
        data: {
            nome_email: nome_email,
            senha: senha,
            captcha: grecaptcha.getResponse()

        },
        success: function (response) {
            $('#teste').html(response);


            // grecaptcha.reset(); // Reset reCaptcha
        }
    })
});


$('.deletar-modal').on('click', function () {
    $('#atualizado-sucesso').addClass('d-none');
});

$('.link-ajaxxx').on('click', function () {

    var link = $(this).attr('destino');
    var id = $(this).attr('idobjeto');

    $(".btn-inverse-danger").trigger('click');

    $.ajax(url + "/weepy/dashboard/admin/" + link + id)
        .done(function (resultado) {
            var msgModal = JSON.parse(resultado);
            console.log(msgModal);
            if (msgModal) {
                var linha = $('#linha' + id);
                linha.hide('slow', function () { linha.remove(); });
                $('html, body').animate({ scrollTop: 0 }, 1000, 'linear');
                $('#atualizado-sucesso').removeClass('d-none');
                $('#atualizado-sucesso .texto-ajax-atualizar').text('Deletado com sucesso');
            } else {
                $('#erro-atualizar').removeClass('d-none');
                $('#erro-atualizar .texto-ajax-erro').text('Erro ao deletar arquivo');
            }
        })
        .fail(function () {

        })
        .always(function () {

        });
});


$('.link-ajax-novo').on('click', function () {

    var link = $(this).attr('link');
    var evento = this;

    $.ajax({
        url: url + "/weepy/dashboard/admin/" + link,
        method: 'post',
        data: new FormData($('#formularios')[0]),
        dataType: "html",
        success: function (resultado) {
            var msgModal = JSON.parse(resultado);
            console.log(msgModal);
            if (msgModal) {
                $('#erro-atualizar').removeClass("d-none");
            }
            $(evento).attr("hidden", false);
        },
        beforeSend: function () {

        },
        complete: function (msg) {

        }

    });

});

