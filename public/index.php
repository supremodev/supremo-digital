<?php

// definir uma constante que mantenha o caminho da pasta do projeto, como "/var/www/html/".
define('ROOT', dirname(__DIR__) . DIRECTORY_SEPARATOR);

// definir uma constante que mantém a pasta "application" do projeto, como "/var/www/html/projeto/application".
define('APP', ROOT . 'app' . DIRECTORY_SEPARATOR);

// Este é o auto-loader para dependências do Composer (para carregar ferramentas em seu projeto).
require ROOT . 'vendor/autoload.php';

// carregar configuração do aplicativo (error reporting etc.)
require APP . 'config/config.php';

use App\Core\Application;

// iniciar a app
$app = new Application();
