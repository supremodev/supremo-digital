<?php

/**
 * Class Funcionarios
 */

namespace App\Model;

use App\Core\Model;

class Funcionario extends Model
{
    /* ########## OBTER TODOS OS FUNCIONIOS DO BANCO DE DADOS ################     */
    public function getAllFuncionario()
    {
        $sql = "SELECT idFunc, 
						funcNome, 
						funcCpf, 
						funcRg, 
						funcEmail, 
						funcSenha, 
						funcFone, 
						funcCel, 
						funcNasci, 
						funcSexo, 
						funcCargo, 
						funcEnd, 
						funcNumero, 
						funcEndComp, 
						funcNivel, 
						funcFoto, 
						funcBiografia, 
						funcStatus 
						FROM funcionario";
        $query = $this->db->prepare($sql);
        $query->execute();

        // fetchAll() é o método PDO que recebe todos os registros retornados, aqui em object-style porque definimos isso em
        // core/controller.php! Se preferir obter um array associativo como resultado, use
        // $query->fetchAll(PDO::FETCH_ASSOC); ou mude as opções em core/controller.php's PDO para
        // $options = array(PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC ...
        return $query->fetchAll();
    }
	
    /* ############ ADICIONAR UM FUNCIONARIO PARA O BANCO ################*/
    public function add($funcNome, 
						$funcCpf, 
						$funcRg, 
						$funcEmail, 
						$funcSenha, 
						$funcFone, 
						$funcCel, 
						$funcNasci, 
						$funcSexo, 
						$funcCargo, 
						$funcEnd, 
						$funcNumero, 
						$funcEndComp, 
						$funcNivel, 
						$funcFoto, 
						$funcAssinatura, 
						$funcBiografia, 
						$funcStatus)
    {
        $sql = "INSERT INTO funcionario	(funcNome, 
										funcCpf, 
										funcRg, 
										funcEmail, 
										funcSenha, 
										funcFone, 
										funcCel, 
										funcNasci, 
										funcSexo, 
										funcCargo, 
										funcEnd, 
										funcNumero, 
										funcEndComp, 
										funcNivel, 
										funcFoto, 
										funcAssinatura, 
										funcBiografia, 
										funcStatus) VALUES (:funcNome, 
															:funcCpf, 
															:funcRg, 
															:funcEmail, 
															:funcSenha, 
															:funcFone, 
															:funcCel, 
															:funcNasci, 
															:funcSexo, 
															:funcCargo, 
															:funcEnd, 
															:funcNumero, 
															:funcEndComp, 
															:funcNivel, 
															:funcFoto, 
															:funcAssinatura, 
															:funcBiografia, 
															:funcStatus)";
        $query = $this->db->prepare($sql);		
		$parameters = array(':funcNome' => $funcNome, 
							':funcCpf' => $funcCpf, 
							':funcRg' => $funcRg, 
							':funcEmail' => $funcEmail, 
							':funcSenha' => $funcSenha, 
							':funcFone' => $funcFone, 
							':funcCel' => $funcCel, 
							':funcNasci' => $funcNasci, 
							':funcSexo' => $funcSexo, 
							':funcCargo' => $funcCargo, 
							':funcEnd' => $funcEnd, 
							':funcNumero' => $funcNumero, 
							':funcEndComp' => $funcEndComp, 
							':funcNivel' => $funcNivel, 
							':funcFoto' => $funcFoto, 
							':funcAssinatura' => $funcAssinatura, 
							':funcBiografia' => $funcBiografia, 
							':funcStatus' => $funcStatus);

        // útil para debugar: você pode ver o SQL atrás da construção usando:
        // echo '[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parameters);  exit();

        $query->execute($parameters);
    }

    /*#################  DESATIVAR UM FUNCIONARIO DO BANCO DE DADOS ####################*/
    public function desativar($funcionario_id)
    {
        $sql = "UPDATE funcionario SET funcStatus = '0' WHERE funcionario.idFunc = :funcionario_id";
        $query = $this->db->prepare($sql);
        $parameters = array(':funcionario_id' => $funcionario_id);

        // útil para debugar: você pode ver o SQL atrás da construção usando:
        // echo '[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parameters);  exit();

        $query->execute($parameters);
    }

	
    /*############# RECEBER UM FUNCIONARIO DO BANCO ######################*/
    public function getFuncionario($funcionario_id)
    {
        $sql = "SELECT 	idFunc, 
						funcNome, 
						funcCpf, 
						funcRg, 
						funcEmail, 
						funcSenha, 
						funcFone, 
						funcCel, 
						funcNasci, 
						funcSexo, 
						funcCargo, 
						funcEnd, 
						funcNumero, 
						funcEndComp, 
						funcNivel, 
						funcFoto, 
						funcAssinatura, 
						funcBiografia, 
						funcStatus FROM funcionario WHERE idFunc = :funcionario_id LIMIT 1";
        $query = $this->db->prepare($sql);
        $parameters = array('funcionario_id' => $funcionario_id);

        // útil para debugar: você pode ver o SQL atrás da construção usando:
        // echo '[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parameters);  exit();

        $query->execute($parameters);

        // fetch() é o método do PDO que recebe exatamente um registro
        return ($query->rowcount() ? $query->fetch() : false);
    }

    /*####################### ATUALIZAR UM FUNCIONARIO NO BANCO ######################## */
    public function update(	$funcNome, 
							$funcCpf, 
							$funcRg, 
							$funcEmail, 
							$funcSenha, 
							$funcFone, 
							$funcCel, 
							$funcNasci, 
							$funcSexo, 
							$funcCargo, 
							$funcEnd, 
							$funcNumero, 
							$funcEndComp, 
							$funcNivel, 
							$funcFoto, 
							$funcAssinatura, 
							$funcBiografia, 
							$funcStatus, $funcionario_id)
    {
        $sql = "UPDATE funcionario SET 	funcNome = :funcNome, 
										funcCpf = :funcCpf, 
										funcRg = :funcRg, 
										funcEmail = :funcEmail, 
										funcSenha = :funcSenha, 
										funcFone = :funcFone, 
										funcCel = :funcCel, 
										funcNasci = :funcNasci, 
										funcSexo = :funcSexo, 
										funcCargo = :funcCargo, 
										funcEnd = :funcEnd, 
										funcNumero = :funcNumero, 
										funcEndComp = :funcEndComp, 
										funcNivel = :funcNivel, 
										funcFoto = :funcFoto, 
										funcAssinatura = :funcAssinatura, 
										funcBiografia = :funcBiografia, 
										funcStatus = :funcStatus WHERE idFunc = :funcionario_id";
		 

        $query = $this->db->prepare($sql);
        $parameters = array(':funcNome' => $funcNome, 
							':funcCpf' => $funcCpf, 
							':funcRg' => $funcRg, 
							':funcEmail' => $funcEmail, 
							':funcSenha' => $funcSenha, 
							':funcFone' => $funcFone, 
							':funcCel' => $funcCel, 
							':funcNasci' => $funcNasci, 
							':funcSexo' => $funcSexo, 
							':funcCargo' => $funcCargo, 
							':funcEnd' => $funcEnd, 
							':funcNumero' => $funcNumero, 
							':funcEndComp' => $funcEndComp, 
							':funcNivel' => $funcNivel, 
							':funcFoto' => $funcFoto, 
							':funcAssinatura' => $funcAssinatura, 
							':funcBiografia' => $funcBiografia, 
							':funcStatus' => $funcStatus, ':funcionario_id' => $funcionario_id);

        // útil para debugar: você pode ver o SQL atrás da construção usando:
        // echo '[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parameters);  exit();

        $query->execute($parameters);
    }

    /*################### "ESTATÍSTICAS" QTD DE FUNCIONÁRIO ################# */
    public function getAmountOfFuncionario()
    {
        $sql = "SELECT COUNT(idFunc) AS amount_of_funcionario FROM funcionario";
        $query = $this->db->prepare($sql);
        $query->execute();

        // fetch() é o método do PDO que recebe exatamente um registro
        return $query->fetch()->amount_of_funcionario;
    }
	
	
	
	/* ################ LISTAR OS PROFISSIONAL ############################### */
    public function getProfissional()
    {
        $sql = "SELECT 	idFunc, 
						funcNome, 
						funcCpf, 
						funcRg, 
						funcEMail, 
						funcSenha, 
						funcFone, 
						funcCel, 
						funcNasci, 
						funcSexo, 
						funcCargo, 
						funcEnd, 
						funcNumero, 
						funcEndComp, 
						funcNivel, 
						funcFoto, 
						funcAssinatura, 
						funcBiografia, 
						funcStatus 
						FROM funcionario WHERE funcCargo = 'Profissional'";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }
	
	/*################ RECEBER UM FUNCIONARIO DO BANCO PELO ID ##########################*/
    public function getPerfilProfissional($funcionario_id)
    {
        $sql = "SELECT * FROM funcionario WHERE idFunc = :funcionario_id LIMIT 1";
        $query = $this->db->prepare($sql);
        $parameters = array('funcionario_id' => $funcionario_id);
		$query->execute($parameters);
        return ($query->rowcount() ? $query->fetch() : false);
    }
	
	/*#################### RECEBER A ESPECIALIDADE DO FUNCIONARIO DO BANCO ####################*/
    public function getPerfilEspecialidade($funcionario_id)
    {
        $sql = "SELECT 	especialidade.espeNome, 
						especialidade.espeDescicao 
						FROM especialidade INNER JOIN funcionario ON especialidade.idEspecialidade = especialidade.idEspecialidade 
						WHERE funcionario.idFunc = $funcionario_id";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();		        
    }
	
}
