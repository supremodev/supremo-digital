<?php

namespace App\Controller;

use App\Model\Paginas;

class SobreController
{
    public function index()
    {
        date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $DataAtual = date_format($Data,"Y-m-d");       

  		require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/templates/modal-supremo.php';
        require APP . 'view/site/sobre.php';
        require APP . 'view/templates/expertises.php';
        require APP . 'view/templates/analise.php';
		require APP . 'view/templates/footer.php';
    }
}
