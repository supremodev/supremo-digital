<?php

namespace App\Controller;

use App\Model\Paginas;


class PortfolioController
{
    public function index()
    {
        date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $DataAtual = date_format($Data,"Y-m-d");       

  		require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/templates/modal-supremo.php';
        require APP . 'view/site/portfolio.php';
        require APP . 'view/templates/analise.php';
		require APP . 'view/templates/footer.php';
    }
	
	public function clacontabil()
    {
        require APP . 'view/templates/head-pg.php';
        require APP . 'view/templates/header-pg.php';
        require APP . 'view/templates/modal-supremo.php';
        require APP . 'view/portfolio/clacontabil.php';
        require APP . 'view/templates/analise-pg.php';
		require APP . 'view/templates/footer-pg.php';
    }
	
	public function comunicacao()
    {
        require APP . 'view/templates/head-pg.php';
        require APP . 'view/templates/header-pg.php';
        require APP . 'view/templates/modal-supremo.php';
        require APP . 'view/portfolio/comunicacao.php';
        require APP . 'view/templates/analise-pg.php';
		require APP . 'view/templates/footer-pg.php';
    }
	
	public function goldtime()
    {
        require APP . 'view/templates/head-pg.php';
        require APP . 'view/templates/header-pg.php';
        require APP . 'view/templates/modal-supremo.php';
        require APP . 'view/portfolio/goldtime.php';
        require APP . 'view/templates/analise-pg.php';
		require APP . 'view/templates/footer-pg.php';
    }
	
	public function moked()
    {
        require APP . 'view/templates/head-pg.php';
        require APP . 'view/templates/header-pg.php';
        require APP . 'view/templates/modal-supremo.php';
        require APP . 'view/portfolio/moked.php';
        require APP . 'view/templates/analise-pg.php';
		require APP . 'view/templates/footer-pg.php';
    }
	
	public function multiplassolucoes()
    {
        require APP . 'view/templates/head-pg.php';
        require APP . 'view/templates/header-pg.php';
        require APP . 'view/templates/modal-supremo.php';
        require APP . 'view/portfolio/multiplassolucoes.php';
        require APP . 'view/templates/analise-pg.php';
		require APP . 'view/templates/footer-pg.php';
    }
	
	public function woodworkmsm()
    {
        require APP . 'view/templates/head-pg.php';
        require APP . 'view/templates/header-pg.php';
        require APP . 'view/templates/modal-supremo.php';
        require APP . 'view/portfolio/woodworkmsm.php';
        require APP . 'view/templates/analise-pg.php';
		require APP . 'view/templates/footer-pg.php';
    }
	
	
}
