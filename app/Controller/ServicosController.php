<?php

namespace App\Controller;

use App\Model\Paginas;


class ServicosController
{
	/*
    public function index()
    {
        date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $DataAtual = date_format($Data,"Y-m-d");       

  		require APP . 'view/templates/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/templates/modal-supremo.php';
        require APP . 'view/site/portfolio.php';
        require APP . 'view/templates/analise.php';
		require APP . 'view/templates/footer.php';
    }
	*/
	
	public function aplicacaoweb()
    {
        require APP . 'view/templates/head-pg.php';
        require APP . 'view/templates/header-pg.php';
        require APP . 'view/templates/modal-supremo.php';
        require APP . 'view/servicos/aplicacaoweb.php';
        require APP . 'view/templates/analise-pg.php';
		require APP . 'view/templates/footer-pg.php';
    }
	
	public function estrategiaseo()
    {
        require APP . 'view/templates/head-pg.php';
        require APP . 'view/templates/header-pg.php';
        require APP . 'view/templates/modal-supremo.php';
        require APP . 'view/servicos/estrategiaseo.php';
        require APP . 'view/templates/analise-pg.php';
		require APP . 'view/templates/footer-pg.php';
    }
	
	public function siteresponsivo()
    {
        require APP . 'view/templates/head-pg.php';
        require APP . 'view/templates/header-pg.php';
        require APP . 'view/templates/modal-supremo.php';
        require APP . 'view/servicos/siteresponsivo.php';
        require APP . 'view/templates/analise-pg.php';
		require APP . 'view/templates/footer-pg.php';
    }
		
	
}
