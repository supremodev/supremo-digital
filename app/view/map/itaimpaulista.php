<!-- Start Home Banner Two -->
        <div class="home-banner-two regioes">
            <div class="d-table">
                <div class="d-table-cell">
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-lg-7 col-md-6">
                                <div class="main-banner-content">
                                    <h2>Conquiste MAIS CLIENTES com um bom perfil de NEGÓCIO</h2>
                                    <p>Seus clientes poderão Ligar, enviar mensagens pelo WhatsApp ou enviar Email. MAIS INTERAÇÕES significam MAIS NEGÓCIOS para você..</p>
                                </div>
                            </div>

                            <div class="col-lg-5 col-md-6">
                                <div class="banner-form">
									<h2>Vamos Conversa...</h2>
									<form>
										<div class="form-group">
											<input type="text" id="nomeRegiao" class="form-control" placeholder="Nome:">
										</div>

										<div class="form-group">
												<textarea name="mens" id="mensRegiao" class="form-control" cols="30" rows="5" required placeholder="Sua mensagem:"></textarea>
										</div>

										<button onclick="enviarRegiao()" class="btn btn-primary">WhatsApp</button>
									</form>
								</div>
                            </div>
                        </div>
						<h1 class="regiao">Itaim Paulista</h1>
                    </div>
                </div>
            </div>

            <div class="banner-img-wrapper">
                <div class="banner-img-1">
                    <img src="<?php echo URL; ?>assets/img/left-shape.png" alt="image">
                </div>
                
                <div class="banner-img-2">
                    <img src="<?php echo URL; ?>assets/img/right-shape.png" alt="image">
                </div>
            </div>

            <div class="shape-img2"><img src="<?php echo URL; ?>assets/img/shape/2.png" alt="image"></div>
            <div class="shape-img3"><img src="<?php echo URL; ?>assets/img/shape/3.png" alt="image"></div>
            <div class="shape-img5"><img src="<?php echo URL; ?>assets/img/shape/5.png" alt="image"></div>
            <div class="shape-img6"><img src="<?php echo URL; ?>assets/img/shape/6.png" alt="image"></div>
            <div class="shape-img7"><img src="<?php echo URL; ?>assets/img/shape/2.png" alt="image"></div>
            <div class="shape-img8"><img src="<?php echo URL; ?>assets/img/shape/10.png" alt="image"></div>
            <div class="shape-img9"><img src="<?php echo URL; ?>assets/img/shape/7.png" alt="image"></div>
            <div class="shape-img10"><img src="<?php echo URL; ?>assets/img/shape/5.png" alt="image"></div>
            <div class="shape-img11"><img src="<?php echo URL; ?>assets/img/shape/11.png" alt="image"></div>
        </div>
        <!-- End  Home Banner Two -->
	
		<!-- Start Features Section -->
		<section class="features-section">
		  <div class="container">
			<div class="row align-items-center">
			  <div class="col-lg-5 col-md-12">
				<div class="features-content-area"> <span>já dizia o ditado “quem não se adapta está fora”</span>
				  <h3>PERFORMANCE E CRIATIVIDADE</h3>
				  <p>Ter um site responsivo, que se adapte a telas de computadores, celulares e tablets é fator obrigatório para que sua empresa possa impactar o maior número de usuários e onde eles estiverem</p>
				</div>
			  </div>
			  <div class="col-lg-7 col-md-12">
				<div class="row">
				  <div class="col-lg-6 col-md-6 col-sm-6">
					<div class="single-features-item bg-f1eff8">
					  <div class="icon"> <i class="flaticon-seo"></i> </div>
					  <h3>Estratégia de SEO</h3>
					  <p>Melhoramos o posicionamento dele na busca orgânica do Google e nos demais canais da internet.</p>
					</div>
				  </div>
				  <div class="col-lg-6 col-md-6 col-sm-6">
					<div class="single-features-item bg-fbe6d4">
					  <div class="icon"> <i class="flaticon-analytics"></i> </div>
					  <h3>Marketing Digital</h3>
					  <p>Um conteúdo altamente qualitativo e de relevância para gerar atratividade e interatividade.</p>
					</div>
				  </div>
				  <div class="col-lg-6 col-md-6 col-sm-6">
					<div class="single-features-item bg-f0fffc">
					  <div class="icon"> <i class="flaticon-laptop"></i> </div>
					  <h3>Desenvolvimento Web</h3>
					  <p>Desenvolvimento de site responsivo, que se adapte a telas de computadores, </p>
					</div>
				  </div>
				  <div class="col-lg-6 col-md-6 col-sm-6">
					<div class="single-features-item bg-f8e1eb">
					  <div class="icon"> <i class="flaticon-analysis-1"></i> </div>
					  <h3>Motion Graphics</h3>
					  <p>Conte uma história, explique seu serviço, mostre um case e apresente sua empresa de um jeito rápido, dinâmico e inovador. </p>
					</div>
				  </div>
				</div>
			  </div>
			</div>
		  </div>
		  <div class="features-animation">
			<div class="shape-img1"><img src="<?php echo URL; ?>assets/img/shape/8.png" alt="APLICAÇÃO WEB"></div>
			<div class="shape-img2"><img src="<?php echo URL; ?>assets/img/shape/5.png" alt="APLICAÇÃO WEB"></div>
		  </div>
		</section>
		<!-- End Features Section --> 

	
		<!-- MAP AREAS ATENDIDAS SÃO PAULO -->
		<?php require_once("areas-saopaulo.php") ?>
        
        <!-- MAP AREAS ATENDIDAS SÃO PAULO -->

        <!-- Start Services Section -->
        <section class="services-section">
		  <div class="container">
			<div class="section-title"> <span>Serviços</span>
			  <h3>SERVIÇOS SUPREMO DIGITAL</h3>
			</div>
			<div class="row">
			  <div class="col-lg-4 col-md-6 col-sm-6">
				<div class="single-services-box">
				  <div class="icon bg-faddd4"> <i class="flaticon-landing-page"></i> </div>
				  <h3>APLICAÇÃO WEB</h3>
				  <p>Uma <strong>aplicação web</strong> sob medida para você alcançar grandes resultados. Apoiamos você na solução de Problemas, aumento de Produtividade e na melhoria de Gestão.</p>
				</div>
			  </div>
			  <div class="col-lg-4 col-md-6 col-sm-6">
				<div class="single-services-box">
				  <div class="icon bg-cafbf1"> <i class="flaticon-goal"></i> </div>
				  <h3>SITES RESPONSIVOS</h3>
				  <p>Ter um <strong>site responsivo</strong>, que se adapte a telas de computadores, celulares e tablets é fator obrigatório para que sua empresa possa impactar o maior número de usuários e onde eles estiverem.</p>
				</div>
			  </div>
			  <div class="col-lg-4 col-md-6 col-sm-6">
				<div class="single-services-box">
				  <div class="icon bg-ddd5fb"> <i class="flaticon-contract"></i> </div>
				  <h3>PROJETOS GRÁFICOS</h3>
				  <p>Uma das formas mais comuns de uma marca ser lembrada é pela sua identidade visual. As pessoas lembram das cores, da logomarca e os associam a empresa. Criamos essa identificação de forma personalizada e a integramos em todas as soluções que o <strong>marketing digital</strong> propõe.</p>
				</div>
			  </div>
			  <div class="col-lg-4 col-md-6 col-sm-6">
				<div class="single-services-box">
				  <div class="icon bg-fcdeee"> <i class="flaticon-application"></i> </div>
				  <h3>OTIMIZAÇÃO DE SITES (SEO)</h3>
				  <p>Estratégias com o objetivo de potencializar e melhorar o posicionamento do seu site no <strong>GOOGLE</strong> de forma <strong>orgânica</strong> sem ter investimento financeiro para que <strong>seja encontrado</strong> mais facilmente.</p>
				</div>
			  </div>
			  <div class="col-lg-4 col-md-6 col-sm-6">
				<div class="single-services-box">
				  <div class="icon bg-c5ebf9"> <i class="flaticon-seo"></i> </div>
				  <h3>CRIAÇÃO DE BLOGs</h3>
				  <p>A <strong>criação</strong> de um <strong>blog</strong> é sempre optada quando existe a necessidade de fornecer informações periódicas entre sua empresa ou seu nome e o público que deseja atingir, também utilizado para aproximar-se de seu público-alvo.</p>
				</div>
			  </div>
			  <div class="col-lg-4 col-md-6 col-sm-6">
				<div class="single-services-box">
				  <div class="icon bg-f8fbd5"> <i class="flaticon-data-recovery"></i> </div>
				  <h3>MOTION GRAPHICS</h3>
				  <p>APRESENTE SUA EMPRESA DE UM JEITO RÁPIDO, DINÂMICO E INOVADOR. O <strong>vídeo</strong> é uma ferramenta de divulgação essencial que possui uma aceitação e interação cada vez maior com o público.</p>
				</div>
			  </div>
			</div>
		  </div>
		  <div class="default-animation">
			<div class="shape-img1"><img src="<?php echo URL; ?>assets/img/shape/12.svg" alt="OTIMIZAÇÃO DE SITES (SEO)"></div>
			<div class="shape-img2"><img src="<?php echo URL; ?>assets/img/shape/13.svg" alt="OTIMIZAÇÃO DE SITES (SEO)"></div>
			<div class="shape-img3"><img src="<?php echo URL; ?>assets/img/shape/14.png" alt="OTIMIZAÇÃO DE SITES (SEO)"></div>
			<div class="shape-img4"><img src="<?php echo URL; ?>assets/img/shape/15.png" alt="OTIMIZAÇÃO DE SITES (SEO)"></div>
			<div class="shape-img5"><img src="<?php echo URL; ?>assets/img/shape/2.png" alt="OTIMIZAÇÃO DE SITES (SEO)"></div>
		  </div>
		</section>
        <!-- End Services Section -->

        <!-- Start Client Section -->
        <section class="client-section regiaoDepo">
            <div class="container">
                <div class="section-title">
                    <span>DEPOIMENTOS</span>
                    <h3>O Que O Nosso Cliente Diz</h3>
                </div>

                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div class="banner-image"> 
							<img src="<?php echo URL; ?>assets/img/saas-image/main-image.png" class="wow zoomIn" data-wow-delay="0.6s" alt="Criação de Sites"> 
						</div>
                    </div>

                    <div class="col-lg-6">
                        <div class="client-slides owl-carousel owl-theme">
                            <div class="testimonial-single-item">
                                <div class="testimonial-content-text">
                                    <div class="icon">
                                       <i class="flaticon-left-quote regiaoDepoIcon"></i>
                                    </div>

                                    <p>Quero agradecer a Supremo Digital por desenvolver todo projeto da Múltiplas Soluções desde a criação do logo, sites, até mesmo campanhas de marketing estratégicas e de alto impacto e resultados que permitiram o crescimento e sucesso da nossa consultoria. Eu recomendo as soluções de marketing dessa agência. Equipe talentosa e autêntica. Sucesso @supremodigital</p>
                                </div>

                                <div class="testimonial-image">
                                    <img src="<?php echo URL; ?>assets/img/marcos-alves.jpg" alt="image">

                                    <h3>Marcos Alves</h3>
                                    <span>Multiplas Soluções</span>
                                </div>
                            </div>

                            <div class="testimonial-single-item">
                                <div class="testimonial-content-text">
                                    <div class="icon">
                                       <i class="flaticon-left-quote regiaoDepoIcon"></i>
                                    </div>

                                    <p>Gostamos muito do trabalho realizado pela Supremo! - Pelo resultado esperado e atingido de um novo site moderno e autoral e também pelo profissionalismo de toda a equipe.</p>
                                </div>

                                <div class="testimonial-image">
                                    <img src="<?php echo URL; ?>assets/img/eric-cardoso.jpg" alt="image">

                                    <h3>Eric Cardoso</h3>
                                    <span>MOKED</span>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </section>
        <!-- End Client Section -->