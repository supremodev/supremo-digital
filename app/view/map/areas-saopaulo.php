<section class="features-area">
            <div class="container">
                <div class="features-title regioesAtendidas">
                    <span>Itaim Paulista</span>
					<h3>Sempre Conectado - Um sites de alta qualidade é seu cartão de visita</h3>
					<h4>Seja para promover o seu negócio, mostrar seu trabalho, abrir a sua loja virtual ou criar um blog, estamos aqui para te ajudar a realizar novas conquistas</h4>
                </div>

				<div class="row">
				<div class="regiao-slides owl-carousel owl-theme">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="single-features-item bg-b5a2f8">
                            <div class="icon">
                                <i class="flaticon-seo"></i>
                            </div>

                            <h3>CENTRO DE SP</h3>
                            <p class="mapCentro"> Bela Vista, 
								Bom Retiro, 
								Cambuci, 
								Consolação, 
								Higienópolis, 
								Liberdade,
								República, 
								Santa Cecília e 
								Sé</p>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="single-features-item bg-f27e19">
                            <div class="icon">
                                <i class="flaticon-analytics"></i>
                            </div>

                            <h3>ZONA NORTE</h3>
                            <p class="mapNorte"> Anhanguera, 
								Brasilândia, 
								Casa Verde, 
								Cachoeirinha, 
								Freguesia do Ó, 
								Jaçanã, 
								Jaraguá, 
								Limão, 
								Mandaqui, 
								Perus, 
								Pirituba, 
								Santana, 
								São Domingos, 
								Tremembé, 
								Tucuruvi, 
								Vila Maria, 
								Vila Medeiros e 
								Vila Guilherme</p>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="single-features-item bg-b5a2f8">
                            <div class="icon">
                                <i class="flaticon-laptop"></i>
                            </div>

                            <h3>ZONA SUL</h3>
                            <p class="mapSul"> Campo Belo, 
								Campo Limpo, 
								Capão Redondo, 
								Cidade Ademar, 
								Cidade Dutra, 
								Cursino, 
								Grajaú, 
								Itaim Bibi, 
								Ipiranga, 
								Jabaquara, 
								Jardim Ângela, 
								Jardim São Luís, 
								Marsilac, 
								Moema, 
								Morumbi, 
								Parelheiros, 
								Pedreira, 
								Sacomã, 
								Santo Amaro,
								Socorro,
								Saúde, 
								Vila Andrade, 
								Vila Mariana e 
								Vila Olímpia</p>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="single-features-item bg-f27e19">
                            <div class="icon">
                                <i class="flaticon-analysis-1"></i>
                            </div>
                            <h3>ZONA LESTE</h3>
                            <p class="mapLeste"> Água Rasa,
								Aricanduva,
								Artur Alvim,
								Belém,
								Brás,
								Cangaíba,
								Carrão,	
								Cidade Líder,	
								Cidade Tiradentes,	
								Ermelino Matarazzo,	
								Guaianases,	
								Itaim Paulista,	
								Itaquera,	
								Jardim Helena,	
								José Bonifácio,	
								Lajeado,
								Mooca, 	
								Pari,	
								Parque do Carmo,
								Penha,	
								Ponte Rasa,
								São Lucas,	
								São Mateus,	
								São Miguel,	
								São Rafael,	
								Sapopemba,	
								Tatuapé,	
								Vila Curuçá,	
								Vila Formosa,	
								Vila Jacuí, 
								Vila Matilde e	
								Vila Prudente</p>
                        </div>
                    </div>
					
					 <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="single-features-item bg-b5a2f8">
                            <div class="icon">
                                <i class="flaticon-analysis-1"></i>
                            </div>
                            <h3>ZONA OESTE</h3>
                            <p class="mapOeste"> Alto de Pinheiros
								Barra Funda,
								Butantã,
								Jaguará,
								Jaguaré,
								Jardins,
								Jardim Paulista,
								Jardim Europa,
								Jardim América,
								Lapa,
								Morumbi,
								Perdizes,
								Pinheiros,
								Raposo Tavares,
								Rio Pequeno,
								Vila Leopoldina,
								Vila Madalena e Vila Sônia</p>
                        </div>
                    </div>
                </div>
			</div>
            </div>

            <div class="features-animation">
                <div class="shape-img1"><img src="<?php echo URL; ?>assets/img/shape/8.png" alt="image"></div>
                <div class="shape-img2"><img src="<?php echo URL; ?>assets/img/shape/5.png" alt="image"></div>
            </div>
        </section>
