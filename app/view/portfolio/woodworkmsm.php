<!-- Start Page Title Area -->
	<div class="page-title-area">
		<div class="d-table">
			<div class="d-table-cell">
				<div class="container">
					<div class="page-title-content">
						<h2>PORTFÓLIO SUPREMO DIGITAL</h2>
						<ul>
							<li><a href="<?php echo URL; ?>home">Home</a></li>
							<li><a href="<?php echo URL; ?>portfolio">Portfólio</a></li>
							<li>WoodWorkmsm</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Page Title Area --> 

	
	<!-- Start Project Details Area -->
		<section class="project-details-area ptb-100">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="project-details-image">
                            <img src="<?php echo URL; ?>assets/img/portfolio/woodworkmsm-cliente-um.png" alt="Site WoodWorkmsm">
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6">
                        <div class="project-details-image">
                            <img src="<?php echo URL; ?>assets/img/portfolio/woodworkmsm-cliente-dois.png" alt="Site WoodWorkmsm">
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12">
                        <div class="projects-details-desc">
                            <h3>Site WoodWorkmsm</h3>

                            <p>A empresa nasceu com a ideia de mudar todo o conceito de marcenaria/móveis sob medida, estávamos cansados da mesmice, dos padrões, da falta de criatividade e inovação nos projetos, falta de profissionalismo e qualidade! Para mudar esse cenário, resolvemos trazer inovação e o que a de melhor para nossos clientes amigos únicos, com projetos exclusivos e materiais de primeira qualidade. </p>

                            <!--<div class="features-text">
                                <h4><i class="flaticon-check-mark"></i>Depoimento:</h4>
                                <p>Quero agradecer a Supremo Digital por desenvolver todo projeto da Múltiplas Soluções desde a criação do logo, sites, até mesmo campanhas de marketing estratégicas e de alto impacto e resultados que permitiram o crescimento e sucesso da nossa consultoria. Eu recomendo as soluções de marketing dessa agência. Equipe talentosa e autêntica. Sucesso @supremodigital</p>
                            </div>-->


                            <div class="project-details-info"><!--
                                <div class="single-info-box">
                                    <h4>Cliente</h4>
                                    <span>Icaro Sammer</span>
                                </div>-->

                                <div class="single-info-box">
                                    <h4>Linguagem</h4>
                                    <span>HTML, CSS, PHP, JavaScript</span>
                                </div>

                                <!--<div class="single-info-box">
                                    <h4>Share</h4>
                                    <ul class="social">
                                        <li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                        <li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                        <li><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                        <li><a href="#" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
                                    </ul>
                                </div> -->

                                <div class="single-info-box">
                                    <a href="https://woodworkmsm.com.br/" target="_blank" class="default-btn-one">Visualização</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
		<!-- End Project Details Area -->