<!-- Start Page Title Area -->
	<div class="page-title-area">
		<div class="d-table">
			<div class="d-table-cell">
				<div class="container">
					<div class="page-title-content">
						<h2>PORTFÓLIO SUPREMO DIGITAL</h2>
						<ul>
							<li><a href="home">Home</a></li>
							<li><a href="portfolio">Portfólio</a></li>
							<li>Auto Escola GALATAS</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Page Title Area --> 

	
	<!-- Start Project Details Area -->
		<section class="project-details-area ptb-100">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="project-details-image">
                            <img src="<?php echo URL; ?>assets/img/portfolio/autoescolagalatas-cliente-um.png" alt="Site Auto Escola GALATAS">
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6">
                        <div class="project-details-image">
                            <img src="<?php echo URL; ?>assets/img/portfolio/autoescolagalatas-cliente-dois.png" alt="Site Auto Escola GALATAS">
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12">
                        <div class="projects-details-desc">
                            <h3>Site Auto Escola GALATAS</h3>

                            <p>Uma empresa que é cituada em São Paulo na zona leste, a Auto Escola GALATAS chegou no mercado para ser uma nova opção para os clientes da região, desde aqueles que buscam simplesmente uma estrutura diferenciada e primam pelo serviço de excelência. Focada na ética profissional, no ideal de inovação da juventude, na qualidade e agilidade do atendimento, nosso principal objetivo é transformar as atividades práticas e da preparação do condutor em uma experiência agradável, interessante e dinâmica. </p>

                            <!--<div class="features-text">
                                <h4><i class="flaticon-check-mark"></i>Depoimento:</h4>
                                <p>Quero agradecer a Supremo Digital por desenvolver todo projeto da Múltiplas Soluções desde a criação do logo, sites, até mesmo campanhas de marketing estratégicas e de alto impacto e resultados que permitiram o crescimento e sucesso da nossa consultoria. Eu recomendo as soluções de marketing dessa agência. Equipe talentosa e autêntica. Sucesso @supremodigital</p>
                            </div>-->


                            <div class="project-details-info"><!--
                                <div class="single-info-box">
                                    <h4>Cliente</h4>
                                    <span>Icaro Sammer</span>
                                </div>-->

                                <div class="single-info-box">
                                    <h4>Linguagem</h4>
                                    <span>HTML, CSS, PHP, JavaScript</span>
                                </div>

                                <!--<div class="single-info-box">
                                    <h4>Share</h4>
                                    <ul class="social">
                                        <li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                        <li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                        <li><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                        <li><a href="#" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
                                    </ul>
                                </div> -->

                                <div class="single-info-box">
                                    <a href="https://autoescolagalatas.com.br/" target="_blank" class="default-btn-one">Visualização</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
		<!-- End Project Details Area -->