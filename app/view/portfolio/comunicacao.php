<!-- Start Page Title Area -->
	<div class="page-title-area">
		<div class="d-table">
			<div class="d-table-cell">
				<div class="container">
					<div class="page-title-content">
						<h2>PORTFÓLIO SUPREMO DIGITAL</h2>
						<ul>
							<li><a href="<?php echo URL; ?>home">Home</a></li>
							<li><a href="<?php echo URL; ?>portfolio">Portfólio</a></li>
							<li>Motion Graphics</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Page Title Area --> 

	
	<!-- Start Project Details Area -->
		<section class="project-details-area ptb-100">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="project-details-image">
                            <img src="<?php echo URL; ?>assets/img/portfolio/motion-graphics-cliente-um.png" alt="Motion Graphics">
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6">
                        <div class="project-details-image">
                            <img src="<?php echo URL; ?>assets/img/portfolio/motion-graphics-cliente-dois.png" alt="Motion Graphics">
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12">
                        <div class="projects-details-desc">
                            <h3>Motion Graphics</h3>
							<h4>APRESENTE SUA EMPRESA DE UM JEITO RÁPIDO, DINÂMICO E INOVADOR.</h4>

                            <p>O vídeo é uma ferramenta de divulgação essencial que possui uma aceitação e interação cada vez maior com o público. O vídeo pode ter vários fins e destinos, pode servir para um treinamento, para uma apresentação, pode ir para o site da sua empresa, redes sociais e ser divulgado em campanhas de Vídeo Ads no Google, YouTube e em outras plataformas de vídeos.</p>
							
							<p>Um vídeo animado pode ser usado por anos e em vários canais de comunicação diferentes como: Instagram, Facebook, WhatsApp, seu Website, Linkedin, E-mail, Apresentações</p>      


                            <div class="project-details-info"><!--
                                <div class="single-info-box">
                                    <h4>Cliente</h4>
                                    <span>Alessandro Palmeira</span>
                                </div>

                                <div class="single-info-box">
                                    <h4>Linguagem</h4>
                                    <span>HTML, CSS, PHP, JavaScript</span>
                                </div>

                                <!--<div class="single-info-box">
                                    <h4>Share</h4>
                                    <ul class="social">
                                        <li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                        <li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                        <li><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                        <li><a href="#" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
                                    </ul>
                                </div> -->

                                <div class="single-info-box">
                                    <a href="https://www.youtube.com/channel/UCEhYRS1-MiM9kecMOsjlICQ" target="_blank" class="default-btn-one">Visualização</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
		<!-- End Project Details Area -->