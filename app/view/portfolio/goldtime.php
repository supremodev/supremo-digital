<!-- Start Page Title Area -->
	<div class="page-title-area">
		<div class="d-table">
			<div class="d-table-cell">
				<div class="container">
					<div class="page-title-content">
						<h2>PORTFÓLIO SUPREMO DIGITAL</h2>
						<ul>
							<li><a href="<?php echo URL; ?>home">Home</a></li>
							<li><a href="<?php echo URL; ?>portfolio">Portfólio</a></li>
							<li>Gold Time Gestão</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Page Title Area --> 

	
	<!-- Start Project Details Area -->
		<section class="project-details-area ptb-100">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="project-details-image">
                            <img src="<?php echo URL; ?>assets/img/portfolio/gold-time-gestao-cliente-um.png" alt="Site Gold Time Gestão">
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6">
                        <div class="project-details-image">
                            <img src="<?php echo URL; ?>assets/img/portfolio/gold-time-gestao-cliente-dois.png" alt="Site Gold Time Gestão">
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12">
                        <div class="projects-details-desc">
                            <h3>Site Gold Time Gestão</h3>

                            <p>O seu tempo vale OURO. A aplicação Gold Time Gestão tem como principal objetivo realizar a gestão da sua agenda e controlar a fila para um atendimento mais ágil e flexível, tornando a permanência do seu cliente muito mais cômoda. Mais não é só isso, a aplicação Gold Time dispõe na mesma plataforma, um lindo site institucional para divulgar seu espaço com administrativo próprio para gerenciar todo conteúdo: cadastrar cliente, funcionário, serviços e uma ferramenta que automatiza as fotos do seu Instagram direto no site. E todo esse controle na palma da sua mão, o sistema Gold Time Gestão foi totalmente desenvolvido para ser gerenciado pelo celular, o que te da a liberdade de gerenciar seu negócio de onde você estiver! Teste agora mesmo Gold Time Gestão, não perca mais tempo!</p>

                            <!--<div class="features-text">
                                <h4><i class="flaticon-check-mark"></i>Depoimento:</h4>
                                <p>Gostamos muito do trabalho realizado pela Supremo! - Pelo resultado esperado e atingido de um novo site moderno e autoral e também pelo profissionalismo de toda a equipe.</p>
                            </div>-->


                            <div class="project-details-info">
                                <div class="single-info-box">
                                    <h4>Cliente</h4>
                                    <span>Marcos Alves</span>
                                </div>

                                <div class="single-info-box">
                                    <h4>Linguagem</h4>
                                    <span>HTML, CSS, PHP, JavaScript</span>
                                </div>

                                <!--<div class="single-info-box">
                                    <h4>Share</h4>
                                    <ul class="social">
                                        <li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                        <li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                        <li><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                        <li><a href="#" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
                                    </ul>
                                </div> -->

                                <div class="single-info-box">
                                    <a href="https://goldtimegestao.com.br" target="_blank" class="default-btn-one">Visualização</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
		<!-- End Project Details Area -->