<!-- Start Page Title Area -->
	<div class="page-title-area">
		<div class="d-table">
			<div class="d-table-cell">
				<div class="container">
					<div class="page-title-content">
						<h2>PORTFÓLIO SUPREMO DIGITAL</h2>
						<ul>
							<li><a href="<?php echo URL; ?>home">Home</a></li>
							<li><a href="<?php echo URL; ?>portfolio">Portfólio</a></li>
							<li>CL Assessoria Contábil</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Page Title Area --> 

	
	<!-- Start Project Details Area -->
		<section class="project-details-area ptb-100">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="project-details-image">
                            <img src="<?php echo URL; ?>assets/img/portfolio/clacontabil-cliente-um.png" alt="Site CL Assessoria Contábil">
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6">
                        <div class="project-details-image">
                            <img src="<?php echo URL; ?>assets/img/portfolio/clacontabil-cliente-dois.png" alt="Site CL Assessoria Contábil">
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12">
                        <div class="projects-details-desc">
                            <h3>Site CL Assessoria Contábil</h3>

                            <p>Após alguns anos de experiências em escritórios e empresas na área Contábil, atual sócia Claudia Cruz fundou a empresa CL Assessoria Contábil, procurando agregar conhecimento, qualidade, tecnologia e principalmente vontade de vencer.</p>

                            <!--<div class="features-text">
                                <h4><i class="flaticon-check-mark"></i>Depoimento:</h4>
                                <p>Gostamos muito do trabalho realizado pela Supremo! - Pelo resultado esperado e atingido de um novo site moderno e autoral e também pelo profissionalismo de toda a equipe.</p>
                            </div>-->


                            <div class="project-details-info">
                                <div class="single-info-box">
                                    <h4>Cliente</h4>
                                    <span>Claudia Cruz</span>
                                </div>

                                <div class="single-info-box">
                                    <h4>Linguagem</h4>
                                    <span>WordPress</span>
                                </div>

                                <!--<div class="single-info-box">
                                    <h4>Share</h4>
                                    <ul class="social">
                                        <li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                        <li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                        <li><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                        <li><a href="#" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
                                    </ul>
                                </div> -->

                                <div class="single-info-box">
                                    <a href="https://clacontabil.com.br" target="_blank" class="default-btn-one">Visualização</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
		<!-- End Project Details Area -->