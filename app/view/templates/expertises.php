<section class="tab-section">
	  <div class="container">
		<div class="section-title"> <span>EXPERTISES</span>
		  <h3>ESTRATÉGIAS CRIATIVAS COM FOCO EM RESULTADOS.</h3>
		</div>
		<div class="tab boosting-list-tab">
		  <ul class="tabs">
			<li><a href="#"> <i class="flaticon-seo-strategy"></i> <span>Aplicação Web</span> </a></li>
			<li class="bg-eff7e9"><a href="#"> <i class="flaticon-responsive-marketing"></i> <span>Site Responsivo</span> </a></li>
			<li class="bg-fff8f0"><a href="#"> <i class="flaticon-seo-performance"></i> <span>Estratégia de SEO</span> </a></li>
			<li class="bg-ecfaf7"><a href="#"> <i class="flaticon-digital-marketing"></i> <span>Mídias Sociais</span> </a></li>
			<li class="bg-f2f0fb"><a href="#"> <i class="flaticon-game-developing"></i> <span>Motion Graphics</span> </a></li>
			<li class="bg-c5ebf9"><a href="#"> <i class="flaticon-web-design"></i> <span>Projos Gráficos</span> </a></li>
		  </ul>
		  <div class="tab_content">
			<div class="tabs_item">
			  <div class="row align-items-center">
				<div class="col-lg-5">
				  <div class="tab-image"> <img src="<?php echo URL; ?>assets/img/svg/supremo-digital-sistema-web.svg" alt="Supremo Digital Sistema Web"> </div>
				</div>
				<div class="col-lg-7">
				  <div class="content">
					<h2>APLICAÇÃO WEB!!!</h2>
					<p>Uma <strong>aplicação web</strong> na internet implica que todo o seu código-fonte, banco de dados e arquivos estão armazenados em uma plataforma segura na internet. Ou seja, você não precisa instalar o software nas máquinas de sua empresa para acessá-lo, basta entrar no seu navegador de internet (de qualquer computador, ipad ou celular conectado na internet), digitar o endereço da <strong>aplicação</strong>, entrar com o seu login e senha e pronto.</p>
					
					  <p>Os custos do desenvolvimento de uma <strong>aplicação web</strong> acabam sendo mais barato do que sistemas desktop ou aplicativo para celular, além de ter a possibilidade de integrar sistemas da empresa na web. Outra vantagem no desenvolvimento de uma <strong>aplicação web</strong> é que a empresa não precisar investir em infra estrutura, já que existem provedores que por custos mensais interessantes, disponibilizam com qualidade e segurança toda estrutura necessária para hospedagem da <strong>aplicação web</strong>.</p>
				  </div>
				  <div class="tab-btn"> <a href="aplicacao-web.php" class="default-btn-one">leia mais</a> </div>
				</div>
			  </div>
			  <div class="tab-shape"> <img src="<?php echo URL; ?>assets/img/tab/shape.png" alt="aplicação web"> </div>
			</div>
			<div class="tabs_item">
				
			  <div class="row align-items-center">
				<div class="col-lg-5">
				  <div class="tab-image"> <img src="<?php echo URL; ?>assets/img/svg/supremo-digital-site-responsivo.svg" alt="SITES RESPONSIVOS"> </div>
				</div>
				<div class="col-lg-7">
				  <div class="content">
					<h2>SITES RESPONSIVOS</h2>
						<p>Já dizia o ditado “quem não se adapta está fora”. Ter um site responsivo, que se adapte a telas de computadores, celulares e tablets é fator obrigatório para que sua empresa possa impactar o maior número de usuários e onde eles estiverem.</p>
					  	<p>Soluções completas para o desenvolvimento de sites institucionais, comerciais, pessoais, dinâmicos, promocionais e administráveis, integração com redes sociais com excelência no design, na arquitetura da informação, na usabilidade, na otimização para mecanismo de busca, na compatibilidade com múltiplos navegadores e na acessibilidade por dispositivos móveis, como Iphone, Ipad, Smartphones e Tablets em geral.</p>
				  </div>
				  <div class="tab-btn"> <a href="site-responsivo.php" class="default-btn-one">leia mais</a> </div>
				</div>
			  </div>
				
			  <div class="tab-shape"> <img src="<?php echo URL; ?>assets/img/tab/shape.png" alt="SITES RESPONSIVOS"> </div>
			</div>
			<div class="tabs_item">
			  <div class="row align-items-center">
				<div class="col-lg-5">
				  <div class="tab-image"> <img src="<?php echo URL; ?>assets/img/svg/supremo-digital-seo.svg" alt="OTIMIZAÇÃO DE SITES (SEO)"> </div>
				</div>
				<div class="col-lg-7">
				  <div class="content">
					<h2>OTIMIZAÇÃO DE SITES (SEO)</h2>
					<p>SEO ESTRATÉGICO – Os buscadores evoluíram nos últimos anos, o SEO também. Do planejamento aos resultados, é preciso estratégia.</p>
				  	<p>SEO (Search Engine Optimization), ou seja, otimização de sites, consiste de inúmeros métodos e estratégias com o objetivo de aumentar a presença e visibilidade de seu site nos motores de busca, como Google e Bing, por meio das palavras-chave mais relevantes de seus produtos e serviços.</p>
					  
				  </div>
				  
				  <div class="tab-btn"> <a href="estrategia-seo.php" class="default-btn-one">leia mais</a> </div>
				</div>
			  </div>
			  <div class="tab-shape"> <img src="<?php echo URL; ?>assets/img/tab/shape.png" alt="OTIMIZAÇÃO DE SITES (SEO)"> </div>
			</div>
			<div class="tabs_item">
			  <div class="row align-items-center">
				<div class="col-lg-5">
				  <div class="tab-image"> <img src="<?php echo URL; ?>assets/img/svg/supremo-digital-midia-social.svg" alt="Supremo Digital - Mídias Sociais"> </div>
				</div>
				<div class="col-lg-7">
				  <div class="content">
					<h2>PLANEJAMENTO DE MÍDIAS SOCIAIS</h2>
					<p>FACEBOOK, TWITTER, INSTAGRAM, LINKEDIN E YOU TUBE</p>
				  	<p>Com base no público alvo, é desenvolvida uma campanha segmentada. Nas redes sociais é possível escolher o público que quer atingir e, assim, focar diretamente nas pessoas que a empresa quer que se tornem cliente, por isso vale investir nessas ferramentas de rede social.</p>
				  </div>
				  
				 <!-- <div class="tab-btn"> <a href="midia-sociais.php" class="default-btn-one">leia mais</a> </div>-->
				</div>
			  </div>
			  <div class="tab-shape"> <img src="<?php echo URL; ?>assets/img/tab/shape.png" alt="PLANEJAMENTO DE MÍDIAS SOCIAIS"> </div>
			</div>
			<div class="tabs_item">
			  <div class="row align-items-center">
				<div class="col-lg-5">
				  <div class="tab-image"> <img src="<?php echo URL; ?>assets/img/svg/supremo-digital-video-motion-graphics.svg" alt="Supremo Digital Vídeo Motion Graphics"> </div>
				</div>
				<div class="col-lg-7">
				  <div class="content">
					<h2>VÍDEO E MOTION GRAPHICS</h2>
					<p>APRESENTE SUA EMPRESA DE UM JEITO RÁPIDO, DINÂMICO E INOVADOR.</p>
				  	<p>O vídeo é uma ferramenta de divulgação essencial que possui uma aceitação e interação cada vez maior com o público. O vídeo pode ter vários fins e destinos, pode servir para um treinamento, para uma apresentação, pode ir para o site da sua empresa, redes sociais e ser divulgado em campanhas de Vídeo Ads no Google, YouTube e em outras plataformas de vídeos.</p>
				  </div>
				  
			<!--	  <div class="tab-btn"> <a href="motion-graphics.php" class="default-btn-one">leia mais</a> </div>-->
				</div>
			  </div>
			  <div class="tab-shape"> <img src="<?php echo URL; ?>assets/img/tab/shape.png" alt="MOTION GRAPHICS"> </div>
			</div>
			<div class="tabs_item">
			  <div class="row align-items-center">
				<div class="col-lg-5">
				  <div class="tab-image"> <img src="<?php echo URL; ?>assets/img/svg/supremo-digital-projetos-graficos.svg" alt="Supremo Digital Projetos Graficos"> </div>
				</div>
				<div class="col-lg-7">
				  <div class="content">
					<h2>PROJETOS GRÁFICOS</h2>
					<p>CRIAÇÃO DA IDENTIDADE VISUAL DA EMPRESA </p>
				  	<p>Uma das formas mais comuns de uma marca ser lembrada é pela sua identidade visual. As pessoas lembram das cores, da logomarca e os associam a empresa. Criamos essa identificação de forma personalizada e a integramos em todas as soluções que o marketing digital propõe.</p>
				  </div>
				  
				  <!--<div class="tab-btn"> <a href="projetos-graficos.php" class="default-btn-one">leia mais</a> </div> -->
				</div>
			  </div>
			  <div class="tab-shape"> <img src="<?php echo URL; ?>assets/img/tab/shape.png" alt="PROJETOS GRÁFICOS"> </div>
			</div>
		  </div>
		</div>
	  </div>
	</section>