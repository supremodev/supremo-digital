<div class="sidebar-modal">
  <div class="sidebar-modal-inner">
    <div class="sidebar-about-area">
      <div class="title">
        <h2>Supremo Digital</h2>
        <p>Supremo Digital é um estúdio de criação sediada em São Paulo com mais de 10 anos de mercado.  Desenvolvemos trabalhos interligados nas diversas mídias: Design Gráfico, Web Sites, Aplicação Web, Campanhas e Ações de Marketing digital. Nosso objetivo é atender a um grupo personalizado de clientes de forma exclusiva e completa, sem a segmentação profissional realizada por uma agência de publicidade “tradicional”.</p>
      </div>
    </div>
    <div class="sidebar-instagram-feed">
      <h2>Instagram</h2>
      <ul id="instagram">
        
      </ul>
    </div>
    <div class="sidebar-contact-area">
      <div class="contact-info">
        <div class="contact-info-content">
			<h2>
				<a href="tel:+05511988626603">11 988 626 603</a>
				<span>ou</span>
				<a href="mailto:contato@supremodigital.com.br">contato@supremodigital.com.br</a>
			</h2>
          <ul class="social">
		  	<li><a href="https://br.linkedin.com/company/supremo-digital" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
            <li><a href="https://www.instagram.com/supremodigital/" target="_blank"><i class="fab fa-instagram"></i></a></li>
            <li><a href="https://www.youtube.com/channel/UCEhYRS1-MiM9kecMOsjlICQ" target="_blank"><i class="fab fa-youtube"></i></a></li>
            <li><a href="https://www.facebook.com/supremodigital" target="_blank"><i class="fab fa-facebook-f"></i></a></li>  
          </ul>
        </div>
      </div>
    </div>
    <span class="close-btn sidebar-modal-close-btn"><i class="fa fa-times"></i></span> </div>
</div>