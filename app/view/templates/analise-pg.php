<section class="team-section ptb-100">
  <div class="container">
    <section class="analysis-section">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-lg-6 col-md-12">
        <div class="analysis-image"> <img src="<?php echo URL; ?>assets/img/svg/supremo-digital-analis-seo.svg" alt="Análise SEO Gratuita Supremo Digital"> </div>
      </div>
      <div class="col-lg-6 col-md-12">
        <div class="analysis-area-content"> <span>ANÁLISE</span>
          <h3>Obtenha Análise SEO Gratuita</h3>
          <strong>Saiba gratuitamente se a sua página está otimizada para as palavras-chave do seu segmento!</strong> </div>
        <div class="contactForms">
          <form id="contactForm" action="#" method="post">
            <div class="row">
              <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                  <input type="text" name="nome" id="nomeAnalise" class="form-control" required placeholder="Nome:">
                </div>
              </div>
              <div class="col-lg-6 col-sm-6">
                <div class="form-group">
                  <input type="email" name="email" id="emailAnalise" class="form-control" required placeholder="E-mail:">
                </div>
              </div>
              <div class="col-lg-12 col-sm-12">
                <div class="form-group">
                  <input type="text" name="urlSite"  id="urlAnalise" class="form-control" required placeholder="Endereço do seu site: ">
                </div>
              </div>
              <div class="col-lg-12 col-sm-12">
                <div class="form-group">
                  <textarea name="mens" id="mensAnalise" class="form-control textarea-hight" cols="30" rows="2" required placeholder="Palavras do seu segmento"></textarea>
                </div>
              </div>
				<div class="col-lg-12 col-md-12">
					<button onclick="enviarAnalise()" class="send-btn-one">enviar solicitação via whatsApp</button>
				</div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
    
  </div>
  <div class="team-animation">
    <div class="shape-img1"><img src="<?php echo URL; ?>assets/img/shape/7.png" alt="Análise SEO Gratuita Supremo Digital"></div>
    <div class="shape-img2"><img src="<?php echo URL; ?>assets/img/shape/9.png" alt="Análise SEO Gratuita Supremo Digital"></div>
    <div class="shape-img3"><img src="<?php echo URL; ?>assets/img/shape/7.png" alt="Análise SEO Gratuita Supremo Digital"></div>
    <div class="shape-img4"><img src="<?php echo URL; ?>assets/img/shape/9.png" alt="Análise SEO Gratuita Supremo Digital"></div>
    <div class="shape-img5"><img src="<?php echo URL; ?>assets/img/shape/9.png" alt="Análise SEO Gratuita Supremo Digital"></div>
  </div>
</section>