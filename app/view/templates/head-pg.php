<!doctype html>
<html lang="pt-br">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="theme-color" content="#025959">
<meta name="description" content="Supremo Digital é um estúdio de Criação de sites, Marketing digital, Desenvolvimento de Sistema e App, sediada em São Paulo com mais de 12 anos de mercado. Desenvolvemos trabalhos interligados nas diversas mídias: Design Gráfico, Web Sites, Sistema Web, Campanhas e Ações de Marketing digital.">
<meta name="author" content="Alessandro Palmeira | Icaro Sammer">
<meta name="robots" content="index">

<!-- Facebook e Twitter compartilhamento -->
<meta property="og:image" content="Supremo Digital"/>
<meta property="og:url" content="https://supremodigital.com.br/"/>
<meta property="og:locale" content="pt_BR"/>
<meta property="og:type" content="website"/>
<meta property="og:site_name" content="Supremo Digital - Criação de Sites - Desenvolvimento de site - Marketing Digital - Sistema Web"/>
<meta property="og:title" content="Supremo Digital - Criação de sites, Marketing digital, Desenvolvimento de Sistema e App"/>
<meta property="og:description" content="Supremo Digital é um estúdio de Criação de sites, Marketing digital, Desenvolvimento de Sistema e App, sediada em São Paulo com mais de 12 anos de mercado. Desenvolvemos trabalhos interligados nas diversas mídias: Design Gráfico, Web Sites, Sistema Web, Campanhas e Ações de Marketing digital."/>
<meta name="twitter:image" content=""/>
<meta name="twitter:url" content="https://supremodigital.com.br/"/>
<meta name="twitter:card" content="summary_large_image"/>
<meta name="twitter:title" content="Supremo Digital - Criação de Sites - Desenvolvimento de site - Marketing Digital - Sistema Web"/>
<meta name="twitter:description" content="Supremo Digital é um estúdio de Criação de sites, Marketing digital, Desenvolvimento de Sistema e App, sediada em São Paulo com mais de 12 anos de mercado. Desenvolvemos trabalhos interligados nas diversas mídias: Design Gráfico, Web Sites, Sistema Web, Campanhas e Ações de Marketing digital."/>

<!-- Bootstrap Min CSS -->
<link rel="stylesheet" href="<?php echo URL; ?>assets/css/bootstrap.min.css">
<!-- Animate Min CSS -->
<link rel="stylesheet" href="<?php echo URL; ?>assets/css/animate.min.css">
<!-- FlatIcon CSS -->
<link rel="stylesheet" href="<?php echo URL; ?>assets/css/flaticon.css">
<link rel="stylesheet" href="<?php echo URL; ?>assets/fonts/font/flaticon.css">
<!-- Odometer Min CSS -->
<link rel="stylesheet" href="<?php echo URL; ?>assets/css/odometer.min.css">
<!-- Nice Select CSS -->
<link rel="stylesheet" href="<?php echo URL; ?>assets/css/nice-select.css">
<!-- MeanMenu CSS -->
<link rel="stylesheet" href="<?php echo URL; ?>assets/css/meanmenu.css">
<!-- Magnific Popup Min CSS -->
<link rel="stylesheet" href="<?php echo URL; ?>assets/css/magnific-popup.min.css">
<!-- Owl Carousel Min CSS -->
<link rel="stylesheet" href="<?php echo URL; ?>assets/css/owl.carousel.min.css">
<!-- Font Awesome Min CSS -->
<link rel="stylesheet" href="<?php echo URL; ?>assets/css/fontawesome.min.css">
<!-- Style CSS -->
<link rel="stylesheet" href="<?php echo URL; ?>assets/css/style.css">
	
<!-- Responsive CSS -->
<link rel="stylesheet" href="<?php echo URL; ?>assets/css/responsive.css">
<link rel="stylesheet" href="<?php echo URL; ?>assets/css/supremo.css">
	
<title>Supremo Digital - Aplicação web - Criação de Sites - Desenvolvimento de site - Marketing Digital - Aplicação Web</title>

<!-- Favicon -->
<link rel="apple-touch-icon" sizes="57x57" href="<?php echo URL; ?>assets/img/ico/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo URL; ?>assets/img/ico/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo URL; ?>assets/img/ico/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo URL; ?>assets/img/ico/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo URL; ?>assets/img/ico/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo URL; ?>assets/img/ico/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo URL; ?>assets/img/ico/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo URL; ?>assets/img/ico/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo URL; ?>assets/img/ico/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo URL; ?>assets/img/ico/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo URL; ?>assets/img/ico/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="<?php echo URL; ?>assets/img/ico/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo URL; ?>assets/img/ico/favicon-16x16.png">
<link rel="manifest" href="<?php echo URL; ?>assets/img/ico/manifest.json">
<meta name="msapplication-TileColor" content="#025959">
<meta name="msapplication-TileImage" content="<?php echo URL; ?>assets/img/ico/ms-icon-144x144.png">
<meta name="theme-color" content="#025959">
	
	
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-22676614-7"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-22676614-7');
</script>

</head>