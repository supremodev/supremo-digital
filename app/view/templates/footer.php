<section class="footer-section ptb-100">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-6">
        <div class="footer-area">
          <div class="footer-heading">
            <h3>Supremo Digital</h3>
          </div>
          <p>Amamos o que fazemos. Sem medo do que virá em seguida viajamos em nossas ideias e depois as lapidamos para torná-las possíveis. Gostamos do diferente e do inusitado e fazemos tudo com muita criatividade para surpreender e sair do lugar comum. </p>
          <ul class="footer-social">			
			<li><a href="https://br.linkedin.com/company/supremo-digital" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
            <li><a href="https://www.instagram.com/supremodigital/" target="_blank"><i class="fab fa-instagram"></i></a></li>
            <li><a href="https://www.youtube.com/channel/UCEhYRS1-MiM9kecMOsjlICQ" target="_blank"><i class="fab fa-youtube"></i></a></li>
            <li><a href="https://www.facebook.com/supremodigital" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
          </ul>
			
        </div>
      </div>
      <div class="col-lg-2 col-md-6 col-sm-6">
        <div class="footer-item-area">
          <div class="footer-heading">
            <h3>Link Important</h3>
          </div>
          <ul class="footer-quick-links">
            <li><a href="sobre">Sobre</a></li>
            <li><a href="site-responsivo">Site Responsivo</a></li>
            <li><a href="aplicacao-web">Aplicação Web</a></li>
            <li><a href="estrategia-seo">SEO</a></li>
            <li><a href="contato">Contato</a></li>
          </ul>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 col-sm-6">
        <div class="footer-heading">
          <h3>Contato</h3>
        </div>
        <div class="footer-info-contact"> <i class="flaticon-call-answer"></i>
          <h3><a class="fone" href="tel:+05511988626603">11 988 626 603</a></h3>
	  	</div>
        <div class="footer-info-contact"> <i class="flaticon-envelope"></i>
          <h3><a class="linkContato"href="mailto:contato@supremodigital.com.br">contato@supremodigital.com.br</a></h3>
       	</div>
      </div>
    </div>
  </div>
  <div class="default-animation">
    <div class="shape-img1"><img src="assets/img/shape/12.svg" alt="Aplicação Web"></div>
    <div class="shape-img2"><img src="assets/img/shape/13.svg" alt="Aplicação Web"></div>
    <div class="shape-img3"><img src="assets/img/shape/14.png" alt="Aplicação Web"></div>
    <div class="shape-img4"><img src="assets/img/shape/15.png" alt="Aplicação Web"></div>
    <div class="shape-img5"><img src="assets/img/shape/2.png" alt="Aplicação Web"></div>
  </div>
</section>
<!-- End Footer Section --> 

<!-- Start Copy Right Section -->
<div class="copyright-area">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-lg-6 col-md-6">
        <p> <i class="far fa-copyright"></i> 2020 Supremo Digital. Todos os direitos reservados.</p>
      </div>
     
    </div>
  </div>
</div>
<!-- End Copy Right Section --> 

<!-- Start Go Top Section -->
<div class="go-top"> <i class="fas fa-chevron-up"></i> <i class="fas fa-chevron-up"></i> </div>
<!-- End Go Top Section --> 

<!-- jQuery Min JS --> 
<script src="assets/js/jquery.min.js"></script> 
<!-- Popper Min JS --> 
<script src="assets/js/popper.min.js"></script> 
<!-- Bootstrap Min JS --> 
<script src="assets/js/bootstrap.min.js"></script> 
<!-- MeanMenu JS  --> 
<script src="assets/js/jquery.meanmenu.js"></script> 
<!-- Appear Min JS --> 
<script src="assets/js/jquery.appear.min.js"></script> 
<!-- Odometer Min JS --> 
<script src="assets/js/odometer.min.js"></script> 
<!-- Owl Carousel Min JS --> 
<script src="assets/js/owl.carousel.min.js"></script> 
<!-- Magnific Popup Min JS --> 
<script src="assets/js/jquery.magnific-popup.min.js"></script> 
<!-- Nice Select Min JS --> 
<script src="assets/js/jquery.nice-select.min.js"></script> 
<!-- Mixitup Min JS --> 
<script src="assets/js/jquery.mixitup.min.js"></script> 
<!-- WOW Min JS --> 
<script src="assets/js/wow.min.js"></script> 
<!-- Parallax Min JS --> 
<script src="assets/js/parallax.min.js"></script> 
<!-- Ajaxchimp JS --> 
<script src="assets/js/jquery.ajaxchimp.min.js"></script> 
<!-- Form Validator JS --> 
<script src="assets/js/form-validator.min.js"></script> 
<!-- Contact JS --> 
<!-- Lity -->
<script src="assets/js/lity.js"></script> 
<!-- Main JS --> 
<script src="assets/js/main.js"></script>
	
<!-- SUPREMO -->
<script src="assets/js/anima-supremo.js"></script>
<script src="assets/js/whats.js"></script>
<script src="assets/js/wordcloud2.js"></script> 
<!-- SUPREMO ->
<script src="assets/js/palavras.js"></script>
<!-- SUPREMO -->
</body>

</html>