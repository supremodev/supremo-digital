<body>

<!-- Preloader -->
<div class="preloader">
  <div class="loader">
    <div class="shadow"></div>
    <div class="box"></div>
  </div>
</div>
<!-- End Preloader -->	
	
<div class="navbar-area">
	<div class="crimso-responsive-nav">
		<div class="container">
			<div class="crimso-responsive-menu">
				<div class="logo"> 
					<a href="home"> 
						<img src="assets/img/logo-supremo-digital.png" alt="Supremo Digital - Criação de Sites - Desenvolvimento de site - Marketing Digital - Sistema Web"> 
					</a> 
				</div>
			</div>
		</div>
	</div>
	<div class="crimso-nav">
		<div class="container">
		  <nav class="navbar navbar-expand-md navbar-light"> 
			  <a class="navbar-brand" href="home"> 
				  <img src="assets/img/logo-supremo-digital.png" alt="Supremo Digital - Criação de Sites - Desenvolvimento de site - Marketing Digital - Sistema Web"> 
			  </a>

			<div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">

				<ul class="navbar-nav">
					<li class="nav-item"> <a href="home" class="nav-link">Home</a></li>
					<li class="nav-item"> <a href="sobre" class="nav-link">Sobre</a></li>
					<li class="nav-item"> <a href="portfolio" class="nav-link">Portfólio</a></li>
					<li class="nav-item">
						<a href="#" class="nav-link">Serviços 
							<i class="fa fa-plus"></i>
						</a>

						<ul class="dropdown-menu">
							<li class="nav-item">
								<a href="servicos/aplicacaoweb" class="nav-link">Aplicação Web</a>
							</li>
							<li class="nav-item">
								<a href="servicos/siteresponsivo" class="nav-link">Site Responsivo</a>
							</li>
							<li class="nav-item">
								<a href="servicos/estrategiaseo" class="nav-link">Estratégia de SEO</a>
							</li>
						</ul>
					</li>					
					<li class="nav-item"> <a href="map" class="nav-link">Regiões Atendidas</a> </li>
					<li class="nav-item"> <a href="contato" class="nav-link">Contato</a> </li>
				</ul>

				<div class="others-options">
					<div class="burger-menu"> <span></span> <span></span> <span></span></div>
				</div>

			</div>
		  </nav>
		</div>
	</div>
</div>