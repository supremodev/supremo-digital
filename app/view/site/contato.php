<!-- Start Page Title Area -->
<div class="page-title-area">
	<div class="d-table">
		<div class="d-table-cell">
			<div class="container">
				<div class="page-title-content">
					<h2>CONTATO SUPREMO DIGITAL</h2>
					<ul>
						<li><a href="index.php">Home</a></li>
						<li>CONTATO</li>
					</ul>
					<h3 class="whatsTitulo">WhatsApp: 11 988 626 603</h3>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- End Page Title Area -->
	
<!-- Start Contact Area -->
<section class="contact-area ptb-100">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-lg-6 col-md-12">
				<div class="contact-box">
					<div class="icon">
						<i class="fa fa-phone"></i>
					</div>

					<div class="content">
						<h4>WhatsApp</h4>
						<p><a href="#">(+55) 11 988 626 603</a></p>
					</div>
				</div>
			</div>

			<div class="col-lg-6 col-md-12">
				<div class="contact-box">
					<div class="icon">
						<i class="fas fa-envelope"></i>
					</div>

					<div class="content">
						<h4>E-mail</h4>
						<p><a href="#">contato@supremodigital.com.br</a></p>
					</div>
				</div>
			</div>                   

			<div class="col-lg-6">
				<div class="contact-text">
					<h3>QUER GERAR RESULTADO PARA SUA EMPRESA?</h3>
					<p>Amamos o que fazemos. Sem medo do que virá em seguida viajamos em nossas ideias e depois as lapidamos para torná-las possíveis. Gostamos do diferente e do inusitado e fazemos tudo com muita criatividade para surpreender e sair do lugar comum.</p>
					<h3>Vamos conversar? Envie sua mensagem, que entraremos em contato com você.</h3>

					<ul class="social-links">
						<li><a href="https://br.linkedin.com/company/supremo-digital" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
						<li><a href="https://www.instagram.com/supremodigital/" target="_blank"><i class="fab fa-instagram"></i></a></li>
						<li><a href="https://www.youtube.com/channel/UCEhYRS1-MiM9kecMOsjlICQ" target="_blank"><i class="fab fa-youtube"></i></a></li>
						<li><a href="https://www.facebook.com/supremodigital" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
					</ul>
				</div>
			</div>

			<div class="col-lg-6">
				<form id="contactForm" action="#" method="post">
					<div class="row">
						<div class="col-lg-6 col-sm-6">
							<div class="form-group">
								<input type="text" name="nome" id="nomeContato" class="form-control" required placeholder="Nome: ">
							</div>
						</div>

						<div class="col-lg-6 col-sm-6">
							<div class="form-group">
								<input type="email" name="email" id="emailContato" class="form-control" required placeholder="E-mail: ">
							</div>
						</div>

						<div class="col-lg-12 col-sm-12">
							<div class="form-group">
								<input type="text" name="assunto" id="assuntoContato" class="form-control" placeholder="Assunto: ">
							</div>
						</div>

						<div class="col-lg-12 col-md-12">
							<div class="form-group">
								<textarea name="mens" id="mensContato" class="form-control textarea-hight" cols="30" rows="5" required placeholder="Sua mensagem:"></textarea>
							</div>
						</div>

						<div class="col-lg-12 col-md-12">
							<button onclick="enviarContato()" class="send-btn-one">enviar solicitação via whatsApp</button>
						</div>
					</div>
				</form>
			</div><!--
			<a href="index.php" data-lity>
				<button class="send-btn-one">
					TESTE URL
				</button>
			</a> -->
		</div>
	</div>
</section>
<!-- Start Contact Area -->

<!-- Start About Section -->
<section class="about-section">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-lg-6 col-md-12">
        <div class="about-image"> <img src="assets/img/svg/supremo-digital-sobre.svg" alt="Supremo Digital Web Sites"> </div>
      </div>
      <div class="col-lg-6 col-md-12">
        <div class="about-area-content"> <span>sobre</span>
          <h3>SUPREMO DIGITAL</h3>
          <p>Um estúdio de criação sediada em São Paulo com mais de 12 anos de mercado. Desenvolvemos trabalhos interligados nas diversas mídias: Design Gráfico, Web Sites, Campanhas e Ações de Marketing digital. Nosso objetivo é atender a um grupo personalizado de clientes de forma exclusiva e completa, sem a segmentação profissional realizada por uma agência de publicidade “tradicional”.</p>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- End About Section --> 