<!-- Start Page Title Area -->
	<div class="page-title-area">
		<div class="d-table">
			<div class="d-table-cell">
				<div class="container">
					<div class="page-title-content">
						<h2>SOBRE SUPREMO DIGITAL</h2>
						<ul>
							<li><a href="index.php">Home</a></li>
							<li>SOBRE</li>
						</ul>
						<h3 class="whatsTitulo">WhatsApp: 11 988 626 603</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Page Title Area --> 

<section class="services-details-area ptb-100">
	<div class="container">
		<div class="services-details-overview">
			<div class="row align-items-center">
				<div class="col-lg-6 col-md-12">
					<div class="services-details-desc">
						<h3>SUPREMO DIGITAL</h3>
						<p>Supremo Digital é um estúdio de criação sediada em São Paulo com mais de 10 anos de mercado. Desenvolvemos trabalhos interligados nas diversas mídias: Design Gráfico, Web Sites, Sistema Web, Campanhas e Ações de Marketing digital. Nosso objetivo é atender a um grupo personalizado de clientes de forma exclusiva e completa, sem a segmentação profissional realizada por uma agência de publicidade “tradicional”.</p>

						<p>Temos ampla experiência nas áreas de Web Design, Inbound Marketing, Outbound Marketing, Marketing Digital, Motion Graphics, Jogos Digitais, Ativação de Canais, Social Media, Desenvolvimento Web e E-commerce tendo adquirido reconhecimento ao longo de nossa trajetória através de cases de sucesso.</p>
						
						<p>Além disso, os integrantes da SUPREMO DIGITAL desenvolvem atividades artísticas pessoais em desenho, vídeo, fotografia e música. Dispomos de profissionais que atuam como docente em uma das melhores instituições de ensino técnico do Brasil – SENAC.</p>
					</div>
				</div>

				<div class="col-lg-6 col-md-12">
					<div class="services-details-image">
						<img src="assets/img/svg/supremo-digital-pg-sobre.svg" alt="Supremo Digital">
					</div>
				</div>
			</div>
		</div>
		<div class="services-details-overview">
			<div class="row align-items-center">
				<div class="col-lg-12 col-md-12">
					
					<blockquote class="wp-block-quote">
						<p>Amamos o que fazemos. Sem medo do que virá em seguida viajamos em nossas ideias e depois as lapidamos para torná-las possíveis. Gostamos do diferente e do inusitado e fazemos tudo com muita criatividade para surpreender e sair do lugar comum.</p>

						<cite>Alessandro Palmeira</cite>
					</blockquote>
					
				</div>
			</div>
		</div>
		<div class="services-details-overview">
			<div class="row align-items-center">
				<div class="col-lg-6 col-md-12">
					<div class="services-details-image">
						<img src="assets/img/svg/supremo-digital-metodologia.svg" alt="Supremo Digital Metodologia">
					</div>
				</div>

				<div class="col-lg-6 col-md-12">
					<div class="services-details-desc">
						<h3>Metodologia de trabalho</h3>
						<div class="services-details-accordion">
							<ul class="accordion">
								<li class="accordion-item">
									<a class="accordion-title active" href="javascript:void(0)">
										<i class="fas fa-plus"></i>
										PESQUISA E ANÁLISE:
									</a>

									<p class="accordion-content show">Todos os projetos realizados pela <strong>Supremo Digital</strong> partem de um Briefing detalhado com o cliente. É nesta fase que mergulhamos nos problemas da empresa para entender e mapear suas necessidades e objetivos. Analisamos os concorrentes diretos e indiretos. Aplicamos algumas metodologias de análises comparativas que permite listar os pontos fortes e fracos da <strong>presença digital</strong> da sua empresa.</p>
								</li>

								<li class="accordion-item">
									<a class="accordion-title" href="javascript:void(0)">
										<i class="fas fa-plus"></i>
										PLANEJAMENTO E COMUNICAÇÃO: 
									</a>

									<p class="accordion-content">A partir da compreensão da demanda do cliente, os profissionais da <strong>Supremo Digital</strong> se reúnem e discutem as possíveis soluções de comunicação. A pesquisa e a busca de referência são estratégias usadas para enriquecer o processo de planejamento e criação. Pautado sempre pela criatividade, depois de analisar os caminhos possíveis e definir o mais adequado ao problema. A <strong>Supremo Digital</strong> constrói uma proposta de trabalho a ser apresentada ao cliente. Nessa proposta estão incluídos os custos de criação e desenvolvimento.</p>
								</li>

								<li class="accordion-item">
									<a class="accordion-title" href="javascript:void(0)">
										<i class="fas fa-plus"></i>
										DINAMIZANDO O TRABALHO: 
									</a>

									<p class="accordion-content">Aprovada a proposta, a Supremo Digital elabora um cronograma, em que estão previstas as etapas de realização do trabalho e o tempo necessário para sua execução. Essa fase é fundamental para garantir que o cliente tenha seu projeto concretizado dentro do prazo planejado.</p>
								</li>
								
								<li class="accordion-item">
									<a class="accordion-title" href="javascript:void(0)">
										<i class="fas fa-plus"></i>
										CRIANDO PARA SOLUCIONAR: 
									</a>

									<p class="accordion-content">Todo o processo de criação é pensado para solucionar o problema de comunicação apresentado, de forma criativa e assertiva, otimizando ao máximo a verba disponível. Além disso, são rigorosamente respeitadas a identidade visual e a cultura do cliente.</p>
								</li>
								
								<li class="accordion-item">
									<a class="accordion-title" href="javascript:void(0)">
										<i class="fas fa-plus"></i>
										APRESENTANDO A SOLUÇÃO:  
									</a>

									<p class="accordion-content">Após o desenvolvimento da solução e a aprovação do cliente, entramos em fase de implementação. Realizamos testes unitários, testes de integração, testes de qualidade. Certificamo-nos que todos os requisitos foram preenchidos. Garantimos que a solução está pronta para ser lançada.</p>
								</li>
								
								<li class="accordion-item">
									<a class="accordion-title" href="javascript:void(0)">
										<i class="fas fa-plus"></i>
										ACOMPANHAMENTO DO CLIENTE:  
									</a>

									<p class="accordion-content">A era digital não para, e para nós cada projeto é como se se tratasse de um sistema vivo. Os consumidores estão cada vez mais exigentes e novas funcionalidades e recursos surgem a todo momento. Realizamos um trabalho de acompanhamento do cliente com a intenção de prever futuras necessidades e garantir que ele seja sempre um dos pioneiros na sua área de atuação.</p>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>   
	</div>
</section> 