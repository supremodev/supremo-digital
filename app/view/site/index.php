<!-- Start Main Banner Area -->
<div class="main-banner">
  <div class="d-table">
    <div class="d-table-cell">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-lg-6 col-md-12">
            <div class="main-banner-content">
              <h1>Supremo Digital</h1>
              <p>Amamos o que fazemos. Sem medo do que virá em seguida viajamos em nossas ideias e depois as lapidamos para torná-las possíveis. Gostamos do diferente e do inusitado e fazemos tudo com muita criatividade para surpreender e sair do lugar comum.</p>
              <div class="banner-btn"> <a href="sobre.php" class="default-btn-one">mais um pouco</a> <a href="https://www.youtube.com/watch?v=GHDD8gY7Wlw" class="video-btn popup-youtube">um vídeo rápido<i class="flaticon-play-button"></i></a> </div>
			
            </div>
          </div>
          <div class="col-lg-6 col-md-12">
            <div class="banner-image"> 
				<img src="assets/img/saas-image/arrow.png" class="wow fadeInLeft" data-wow-delay="0.6s" alt="Criação de Sites"> 
				<img src="assets/img/saas-image/box1.png" class="wow fadeInUp" data-wow-delay="0.6s" alt="Criação de Sites"> 
				<img src="assets/img/saas-image/boy1.png" class="wow fadeInLeft" data-wow-delay="0.6s" alt="Criação de Sites"> 
				<img src="assets/img/saas-image/boy2.png" class="wow zoomIn" data-wow-delay="0.6s" alt="Criação de Sites"> 
				<img src="assets/img/saas-image/boy3.png" class="wow bounceIn" data-wow-delay="0.6s" alt="Criação de Sites"> 
				<img src="assets/img/saas-image/digital-screen.png" class="wow fadeInDown" data-wow-delay="0.6s" alt="Criação de Sites"> 
				<img src="assets/img/saas-image/filter1.png" class="wow zoomIn" data-wow-delay="0.6s" alt="Criação de Sites"> 
				<img src="assets/img/saas-image/filter2.png" class="wow fadeInUp" data-wow-delay="0.6s" alt="Criação de Sites"> 
				<img src="assets/img/saas-image/filter3.png" class="wow rotateIn" data-wow-delay="0.6s" alt="Criação de Sites"> 
				<img src="assets/img/saas-image/girl1.png" class="wow fadeInUp" data-wow-delay="0.6s" alt="Criação de Sites"> 
				<img src="assets/img/saas-image/girl2.png" class="wow zoomIn" data-wow-delay="0.6s" alt="Criação de Sites"> 
				<img src="assets/img/saas-image/monitor.png" class="wow fadeInRight" data-wow-delay="0.6s" alt="Criação de Sites"> 
				<img src="assets/img/saas-image/4.png" class="wow zoomIn" data-wow-delay="0.6s" alt="Criação de Sites">
				<img src="assets/img/saas-image/whats.png" class="wow zoomIn" data-wow-delay="0.6s" alt="Criação de Sites">
				<img src="assets/img/saas-image/7.png" class="wow zoomIn" data-wow-delay="0.6s" alt="Criação de Sites"> 
				<img src="assets/img/saas-image/main-image.png" class="wow zoomIn" data-wow-delay="0.6s" alt="Criação de Sites"> </div>
          </div>
        </div>
        <div class="banner-bg-text">APLICAÇÃO</div>
      </div>
    </div>
  </div>
  <div class="shape-img1"><img src="assets/img/shape/1.png" alt="APLICAÇÃO WEB"></div>
  <div class="shape-img2"><img src="assets/img/shape/2.png" alt="APLICAÇÃO WEB"></div>
  <div class="shape-img3"><img src="assets/img/shape/3.png" alt="APLICAÇÃO WEB"></div>
  <div class="shape-img4"><img src="assets/img/shape/4.png" alt="APLICAÇÃO WEB"></div>
  <div class="shape-img5"><img src="assets/img/shape/5.png" alt="APLICAÇÃO WEB"></div>
  <div class="shape-img6"><img src="assets/img/shape/6.png" alt="APLICAÇÃO WEB"></div>
  <div class="shape-img7"><img src="assets/img/shape/2.png" alt="APLICAÇÃO WEB"></div>
  <div class="shape-img8"><img src="assets/img/shape/10.png" alt="APLICAÇÃO WEB"></div>
  <div class="shape-img9"><img src="assets/img/shape/7.png" alt="APLICAÇÃO WEB"></div>
  <div class="shape-img10"><img src="assets/img/shape/5.png" alt="APLICAÇÃO WEB"></div>
  <div class="shape-img11"><img src="assets/img/shape/11.png" alt="APLICAÇÃO WEB"></div>
</div>
<!-- End Main Banner Area --> 

<!-- Start Features Section -->
<section class="features-section">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-lg-5 col-md-12">
        <div class="features-content-area"> <span>já dizia o ditado “quem não se adapta está fora”</span>
          <h3>PERFORMANCE E CRIATIVIDADE</h3>
          <p>Ter um site responsivo, que se adapte a telas de computadores, celulares e tablets é fator obrigatório para que sua empresa possa impactar o maior número de usuários e onde eles estiverem</p>
        </div>
      </div>
      <div class="col-lg-7 col-md-12">
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="single-features-item bg-f1eff8">
              <div class="icon"> <i class="flaticon-seo"></i> </div>
              <h3>Estratégia de SEO</h3>
              <p>Melhoramos o posicionamento dele na busca orgânica do Google e nos demais canais da internet.</p>
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="single-features-item bg-fbe6d4">
              <div class="icon"> <i class="flaticon-analytics"></i> </div>
              <h3>Marketing Digital</h3>
              <p>Um conteúdo altamente qualitativo e de relevância para gerar atratividade e interatividade.</p>
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="single-features-item bg-f0fffc">
              <div class="icon"> <i class="flaticon-laptop"></i> </div>
              <h3>Desenvolvimento Web</h3>
              <p>Desenvolvimento de site responsivo, que se adapte a telas de computadores, </p>
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="single-features-item bg-f8e1eb">
              <div class="icon"> <i class="flaticon-analysis-1"></i> </div>
              <h3>Motion Graphics</h3>
              <p>Conte uma história, explique seu serviço, mostre um case e apresente sua empresa de um jeito rápido, dinâmico e inovador. </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="features-animation">
    <div class="shape-img1"><img src="assets/img/shape/8.png" alt="APLICAÇÃO WEB"></div>
    <div class="shape-img2"><img src="assets/img/shape/5.png" alt="APLICAÇÃO WEB"></div>
  </div>
</section>
<!-- End Features Section --> 

<!-- Start About Section -->
<section class="about-section">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-lg-6 col-md-12">
        <div class="about-image"> <img src="assets/img/svg/supremo-digital-sobre.svg" alt="Supremo Digital Web Sites"> </div>
      </div>
      <div class="col-lg-6 col-md-12">
        <div class="about-area-content"> <span>sobre</span>
          <h3>SUPREMO DIGITAL</h3>
          <p>Um estúdio de criação sediada em São Paulo com mais de 12 anos de mercado. Desenvolvemos trabalhos interligados nas diversas mídias: Aplicação Web, Design Gráfico, Web Sites, Campanhas e Ações de Marketing digital. Nosso objetivo é atender a um grupo personalizado de clientes de forma exclusiva e completa, sem a segmentação profissional realizada por uma agência de publicidade “tradicional”.</p>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- End About Section --> 

<!-- Start Services Section -->
<section class="services-section">
  <div class="container">
    <div class="section-title"> <span>Serviços</span>
      <h3>SERVIÇOS SUPREMO DIGITAL</h3>
    </div>
    <div class="row">
      <div class="col-lg-4 col-md-6 col-sm-6">
        <div class="single-services-box">
          <div class="icon bg-faddd4"> <i class="flaticon-landing-page"></i> </div>
          <h3>APLICAÇÃO WEB</h3>
          <p>Uma <strong>aplicação web</strong> sob medida para você alcançar grandes resultados. Apoiamos você na solução de Problemas, aumento de Produtividade e na melhoria de Gestão.</p>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 col-sm-6">
        <div class="single-services-box">
          <div class="icon bg-cafbf1"> <i class="flaticon-goal"></i> </div>
          <h3>SITES RESPONSIVOS</h3>
          <p>Ter um <strong>site responsivo</strong>, que se adapte a telas de computadores, celulares e tablets é fator obrigatório para que sua empresa possa impactar o maior número de usuários e onde eles estiverem.</p>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 col-sm-6">
        <div class="single-services-box">
          <div class="icon bg-ddd5fb"> <i class="flaticon-contract"></i> </div>
          <h3>PROJETOS GRÁFICOS</h3>
          <p>Uma das formas mais comuns de uma marca ser lembrada é pela sua identidade visual. As pessoas lembram das cores, da logomarca e os associam a empresa. Criamos essa identificação de forma personalizada e a integramos em todas as soluções que o <strong>marketing digital</strong> propõe.</p>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 col-sm-6">
        <div class="single-services-box">
          <div class="icon bg-fcdeee"> <i class="flaticon-application"></i> </div>
          <h3>OTIMIZAÇÃO DE SITES (SEO)</h3>
          <p>Estratégias com o objetivo de potencializar e melhorar o posicionamento do seu site no <strong>GOOGLE</strong> de forma <strong>orgânica</strong> sem ter investimento financeiro para que <strong>seja encontrado</strong> mais facilmente.</p>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 col-sm-6">
        <div class="single-services-box">
          <div class="icon bg-c5ebf9"> <i class="flaticon-seo"></i> </div>
          <h3>CRIAÇÃO DE BLOGs</h3>
          <p>A <strong>criação</strong> de um <strong>blog</strong> é sempre optada quando existe a necessidade de fornecer informações periódicas entre sua empresa ou seu nome e o público que deseja atingir, também utilizado para aproximar-se de seu público-alvo.</p>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 col-sm-6">
        <div class="single-services-box">
          <div class="icon bg-f8fbd5"> <i class="flaticon-data-recovery"></i> </div>
          <h3>MOTION GRAPHICS</h3>
          <p>APRESENTE SUA EMPRESA DE UM JEITO RÁPIDO, DINÂMICO E INOVADOR. O <strong>vídeo</strong> é uma ferramenta de divulgação essencial que possui uma aceitação e interação cada vez maior com o público.</p>
        </div>
      </div>
    </div>
  </div>
  <div class="default-animation">
    <div class="shape-img1"><img src="assets/img/shape/12.svg" alt="OTIMIZAÇÃO DE SITES (SEO)"></div>
    <div class="shape-img2"><img src="assets/img/shape/13.svg" alt="OTIMIZAÇÃO DE SITES (SEO)"></div>
    <div class="shape-img3"><img src="assets/img/shape/14.png" alt="OTIMIZAÇÃO DE SITES (SEO)"></div>
    <div class="shape-img4"><img src="assets/img/shape/15.png" alt="OTIMIZAÇÃO DE SITES (SEO)"></div>
    <div class="shape-img5"><img src="assets/img/shape/2.png" alt="OTIMIZAÇÃO DE SITES (SEO)"></div>
  </div>
</section>
<!-- End Services Section --> 

<!-- Start Choose Section -->
<section class="choose-section">
  
</section>
<!-- End Choose Section --> 

 			