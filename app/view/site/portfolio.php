<!-- Start Page Title Area -->
	<div class="page-title-area">
		<div class="d-table">
			<div class="d-table-cell">
				<div class="container">
					<div class="page-title-content">
						<h2>PORTFÓLIO SUPREMO DIGITAL</h2>
						<ul>
							<li><a href="index.php">Home</a></li>
							<li>PORTFÓLIO</li>
							<h3 class="whatsTitulo">WhatsApp: 11 988 626 603</h3>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Page Title Area --> 

	
	<!-- Start Portfolio Section -->
        <section class="portfolio-section pt-100">
            <div class="container">
                <div class="section-title">
                    <span>CONHEÇA ALGUNS DE NOSSOS PROJETOS!</span>
					<p>Amamos o que fazemos. Sem medo do que virá em seguida viajamos em nossas ideias e depois as lapidamos para torná-las possíveis</p>
                </div>

                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <div class="single-portfolio-box">
                            <div class="portfolio-image">
                                <img src="assets/img/portfolio/gold-tima-gestao-aplicação-web.png" alt="Gold Time Gestão">
                            </div>

                            <div class="portfolio-hover-content">
                                <h3>
                                    <a href="portfolio/goldtime">Aplicação Gold Time Gestão</a>
                                </h3>
                            </div>
                        </div>    

                        <div class="single-portfolio-box">
                            <div class="portfolio-image">
                                <img src="assets/img/portfolio/cl-assessoria-contabil.png" alt=" Site CL Assessoria Contábil">
                            </div>

                            <div class="portfolio-hover-content">
                                <h3>
                                    <a href="portfolio/clacontabil"> Site CL Assessoria Contábil</a>
                                </h3>
                            </div>
                        </div>  
                    </div>

                    <div class="col-lg-4 col-md-6">
                        <div class="single-portfolio-box">
                            <div class="portfolio-image">
                                <img src="assets/img/portfolio/moked-do-brasil.png" alt="Moked do Brasil">
                            </div>

                            <div class="portfolio-hover-content">
                                <h3>
                                    <a href="portfolio/moked"> Site Moked</a>
                                </h3>
                            </div>
                        </div>  

                        <div class="single-portfolio-box">
                            <div class="portfolio-image">
                                <img src="assets/img/portfolio/multiplas-solucoes.png" alt="múltiplas soluções">
                            </div>

                            <div class="portfolio-hover-content">
                                <h3>
                                    <a href="portfolio/multiplassolucoes"> Site Múltiplas Soluções</a>
                                </h3>
                            </div>
                        </div>  

                        <div class="single-portfolio-box">
                            <div class="portfolio-image">
                                <img src="assets/img/portfolio/escola-galatas.png" alt="Auto Escola GALATAS">
                            </div>

                            <div class="portfolio-hover-content">
                                <h3>
                                    <a href="portfolio/autoescolagalatas"> Site Auto Escola GALATAS</a>
                                </h3>
                            </div>
                        </div>  
                    </div>

                    <div class="col-lg-4 col-md-12">
                        <div class="single-portfolio-box">
                            <div class="portfolio-image">
                                <img src="assets/img/portfolio/woodwork.png" alt="woodwork">
                            </div>

                            <div class="portfolio-hover-content">
                                <h3>
                                    <a href="portfolio/woodworkmsm"> Site WOODWORK</a>
                                </h3>
                            </div>
                        </div>  

                        <div class="single-portfolio-box">
                            <div class="portfolio-image">
                                <img src="assets/img/portfolio/motion-graphics-supremo-digital.png" alt="Motion Graphics">
                            </div>

                            <div class="portfolio-hover-content">
                                <h3>
                                    <a href="portfolio/comunicacao">Vinheta em Motion Graphics</a>
                                </h3>
                            </div>
                        </div>  
                    </div>
                </div>
            </div>
        </section>
        <!-- End Portfolio Section -->