<link rel="stylesheet" href="<?php echo URL; ?>assets/css/banner-sistema-web.css">
<!-- Start Main Banner Area -->
<div class="main-banner">
  <div class="d-table">
    <div class="d-table-cell">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-lg-6 col-md-12">
            <div class="main-banner-content">
              <h1>APLICAÇÃO WEB</h1>
              <p>Uma aplicação web sob medida para você alcançar grandes resultados. Apoiamos você na solução de Problemas, aumento de Produtividade e na melhoria de Gestão.</p>			
            </div>
          </div>
          <div class="col-lg-6 col-md-12">
            <div class="banner-image banner-sistema-web"> 
				<img src="<?php echo URL; ?>assets/img/svg/anima-sistema-web/fundo-grid.png" class="wow fadeInLeft" data-wow-delay="0.6s" alt="APLICAÇÃO WEB"> 
				<img src="<?php echo URL; ?>assets/img/svg/anima-sistema-web/painel1.png" class="wow fadeInUp" data-wow-delay="0.6s" alt="APLICAÇÃO WEB"> 
				<img src="<?php echo URL; ?>assets/img/svg/anima-sistema-web/painel2.png" class="wow fadeInLeft" data-wow-delay="0.8s" alt="APLICAÇÃO WEB"> 
				<img src="<?php echo URL; ?>assets/img/svg/anima-sistema-web/painel3.png" class="wow zoomIn" data-wow-delay="0.7s" alt="APLICAÇÃO WEB"> 
				<img src="<?php echo URL; ?>assets/img/svg/anima-sistema-web/celula.png" class="wow bounceIn" data-wow-delay="0.7s" alt="APLICAÇÃO WEB"> 
				<img src="<?php echo URL; ?>assets/img/svg/anima-sistema-web/cubo1.png" class="wow fadeInDown" data-wow-delay="0.8s" alt="APLICAÇÃO WEB"> 
				<img src="<?php echo URL; ?>assets/img/svg/anima-sistema-web/engrenagem1.png" class="wow zoomIn" data-wow-delay="0.6s" alt="APLICAÇÃO WEB"> 
				<img src="<?php echo URL; ?>assets/img/svg/anima-sistema-web/cubo2.png" class="wow fadeInUp" data-wow-delay="0.9s" alt="APLICAÇÃO WEB"> 
				<img src="<?php echo URL; ?>assets/img/svg/anima-sistema-web/engrenagem2.png" class="wow rotateIn" data-wow-delay="0.6s" alt="APLICAÇÃO WEB"> 
				<img src="<?php echo URL; ?>assets/img/svg/anima-sistema-web/engrenagem3.png" class="wow fadeInUp" data-wow-delay="0.6s" alt="APLICAÇÃO WEB"> 
				<img src="<?php echo URL; ?>assets/img/svg/anima-sistema-web/engrenagem4.png" class="wow zoomIn" data-wow-delay="0.6s" alt="APLICAÇÃO WEB"> 
				<img src="<?php echo URL; ?>assets/img/svg/anima-sistema-web/pc.png" class="wow fadeInRight" data-wow-delay="0.15s" alt="APLICAÇÃO WEB"> 
				<img src="<?php echo URL; ?>assets/img/svg/anima-sistema-web/lupa.png" class="wow zoomIn" data-wow-delay="0.10s" alt="APLICAÇÃO WEB"> 
				<img src="<?php echo URL; ?>assets/img/svg/anima-sistema-web/end-web.png" class="wow fadeInUp" data-wow-delay="0.9s" alt="APLICAÇÃO WEB">
				<img src="<?php echo URL; ?>assets/img/svg/anima-sistema-web/hashtag.png" class="wow fadeInDown" data-wow-delay="0.12s" alt="APLICAÇÃO WEB">
				<img src="<?php echo URL; ?>assets/img/svg/anima-sistema-web/engrenagem5.png" class="wow zoomIn" data-wow-delay="0.6s" alt="APLICAÇÃO WEB">
				<img src="<?php echo URL; ?>assets/img/svg/anima-sistema-web/sistema-web.png" class="wow zoomIn" data-wow-delay="0.6s" alt="APLICAÇÃO WEB"> 
			  </div>
          </div>
        </div>
        <div class="banner-sistema-web-titulo">APLICAÇÃO WEB</div>
      </div>
    </div>
  </div>
  <div class="shape-img1"><img src="<?php echo URL; ?>assets/img/shape/1.png" alt="APLICAÇÃO WEB"></div>
  <div class="shape-img2"><img src="<?php echo URL; ?>assets/img/shape/2.png" alt="APLICAÇÃO WEB"></div>
  <div class="shape-img3"><img src="<?php echo URL; ?>assets/img/shape/3.png" alt="APLICAÇÃO WEB"></div>
  <div class="shape-img4"><img src="<?php echo URL; ?>assets/img/shape/4.png" alt="APLICAÇÃO WEB"></div>
  <div class="shape-img5"><img src="<?php echo URL; ?>assets/img/shape/5.png" alt="APLICAÇÃO WEB"></div>
  <div class="shape-img6"><img src="<?php echo URL; ?>assets/img/shape/6.png" alt="APLICAÇÃO WEB"></div>
  <div class="shape-img7"><img src="<?php echo URL; ?>assets/img/shape/2.png" alt="APLICAÇÃO WEB"></div>
  <div class="shape-img8"><img src="<?php echo URL; ?>assets/img/shape/10.png" alt="APLICAÇÃO WEB"></div>
  <div class="shape-img9"><img src="<?php echo URL; ?>assets/img/shape/7.png" alt="APLICAÇÃO WEB"></div>
  <div class="shape-img10"><img src="<?php echo URL; ?>assets/img/shape/5.png" alt="APLICAÇÃO WEB"></div>
  <div class="shape-img11"><img src="<?php echo URL; ?>assets/img/shape/11.png" alt="APLICAÇÃO WEB"></div>
</div>
<!-- End Main Banner Area --> 
<section class="services-details-area ptb-100">
	<div class="container">
		<div class="services-details-overview">
			<div class="row align-items-center">
				<div class="col-lg-6 col-md-12">
					<div class="services-details-desc">
						<h3>Aplicação ou Sistema WEB?</h3>
						<p>De modo simplificado um Sistema Web é um aplicativo que pode ser acessada por qualquer navegador, seja pelo celular, computador ou tablete. Tanto é que que ele também é chamado de Aplicação Web...</p>
						
						<p>Na realidade um Sistema Web é um tipo de Site dinâmico, onde a experiência do usuário é personalizada. Diferente de um Site estático, você pode ter um login, gerenciar dados, todas as possibilidades de um aplicativo. O <strong>facebook</strong> quando é aberto no navegador é um dos exemplos mais conhecidos de Sistema Web.</p>

						<p>Um sistema web, se define com um software em que aqueles que são cadastrados podem se conectar de qualquer lugar, bastando apenas ter conexão com a intt. Muitas vezes é vantajoso usá-lo como forma de melhorar seu negócio, para isso, é preciso entender seu ramo e como o sistema beneficiará os envolvidos.</p>
						
					</div>
				</div>

				<div class="col-lg-6 col-md-12">
					<div class="services-details-image">
						<img src="<?php echo URL; ?>assets/img/svg/supremo-digital-pg-sobre.svg" alt="Supremo Digital - APLICAÇÃO WEB">
					</div>
				</div>
			</div>
		</div>
		<div class="services-details-overview">
			<div class="row align-items-center">
				<div class="col-lg-12 col-md-12">
					
					<blockquote class="wp-block-quote">
						<p class="frasePg">Agora que você já sabe as diferenças entre um Sistema Web e um Site. Já pensou em qual plataforma sua empresa precisa? Já pensou em desenvolver sua <strong>Aplicação Web?</strong></p>

						<cite>Alessandro Palmeira</cite>
					</blockquote>
					
				</div>
			</div>
		</div>
		<div class="services-details-overview">
			<div class="row align-items-center">
				<div class="col-lg-6 col-md-12">
					<div class="services-details-image">
						<img src="<?php echo URL; ?>assets/img/svg/supremo-digital-aplicacao-web.svg" alt="Supremo Digital Aplicação Web">
					</div>
				</div>

				<div class="col-lg-6 col-md-12">
					<div class="services-details-desc">
						<h3>As vantagens de uma aplicação web</h3>
						<p>Há uma série de benefícios proporcionados pela implementação de uma aplicação web na rotina empresarial:</p>
						<div class="services-details-accordion">
							<ul class="accordion">
								<li class="accordion-item">
									<a class="accordion-title active" href="javascript:void(0)">
										<i class="fas fa-plus"></i>
										Otimizar o tempo na execução de tarefas:
									</a>

									<p class="accordion-content show">Algumas tarefas, ainda que rotineiras, demandam tempo e são burocráticas, estando sujeitas à falhas se não forem executadas com atenção. Uma aplicação web, com todas as suas funcionalidades, reduz o tempo gasto, inclusive por detectar falhas automaticamente, eliminando a necessidade de revisãoe e de erros.</p>
								</li>

								<li class="accordion-item">
									<a class="accordion-title" href="javascript:void(0)">
										<i class="fas fa-plus"></i>
										Gera economia: 
									</a>

									<p class="accordion-content">Uma aplicação web pode ser armazenado em qualquer provedor de hospedagem. Assim sendo, não demanda gastos com infraestrutura. Além disso, por reduzir o tempo gasto com as tarefas e a incidência de erros, gera economia de recursos e permite que os colaboradores estejam livres para desempenhar outras funções, contribuindo para aumento da produtividade.</p>
								</li>

								<li class="accordion-item">
									<a class="accordion-title" href="javascript:void(0)">
										<i class="fas fa-plus"></i>
										É seguro: 
									</a>

									<p class="accordion-content">Por ser hospedado em locais especializados, o banco de dados da aplicação web fica mais seguro do que as aplicações normais. Além disso, conta com certificados de segurança e senhas de acesso criptografadas.</p>
								</li>
								
								<li class="accordion-item">
									<a class="accordion-title" href="javascript:void(0)">
										<i class="fas fa-plus"></i>
										É acessível: 
									</a>

									<p class="accordion-content">Por estar em ambiente web, a aplicação pode ser acessado de qualquer computador ou dispositivo móvel que tenha internet. Assim sendo, colaboradores e gestores podem verificar e administrar dados remotamente e em qualquer horário.</p>
								</li>
								
								<li class="accordion-item">
									<a class="accordion-title" href="javascript:void(0)">
										<i class="fas fa-plus"></i>
										Pode ser personalizado:  
									</a>

									<p class="accordion-content">É possível que se desenvolva uma aplicação web perfeitamente ajustado às necessidades da sua empresa. Isso garante que essa aplicação traga ainda mais vantagens para a rotina organizacional.</p>
								</li>
								
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>   
	</div>
</section> 