<link rel="stylesheet" href="<?php echo URL; ?>assets/css/banner-seo.css">
<!-- Start Main Banner Area -->
<div class="main-banner">
  <div class="d-table">
    <div class="d-table-cell">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-lg-6 col-md-12">
            <div class="main-banner-content">
              <h1>SEO </h1>
              <h2>(Otimização para motores de busca)</h2>			
            </div>
          </div>
          <div class="col-lg-6 col-md-12">
            <div class="banner-image banner-sistema-web"> 
				<img src="<?php echo URL; ?>assets/img/svg/anima-sistema-web/fundo-grid.png" class="wow fadeInLeft" data-wow-delay="0.6s" alt="SEO Otimização para motores de busca">
				
				<img src="<?php echo URL; ?>assets/img/svg/anima-seo/bola.svg" class="wow fadeInUp" data-wow-delay="0.6s" alt="SEO Otimização para motores de busca"> 
				<img src="<?php echo URL; ?>assets/img/svg/anima-seo/grafico1.svg" class="wow rotateIn" data-wow-delay="0.8s" alt="SEO Otimização para motores de busca"> 
				<img src="<?php echo URL; ?>assets/img/svg/anima-seo/grafico4.svg" class="wow zoomIn" data-wow-delay="0.7s" alt="SEO Otimização para motores de busca"> 
				<img src="<?php echo URL; ?>assets/img/svg/anima-seo/grafico2.svg" class="wow bounceIn" data-wow-delay="0.7s" alt="SEO Otimização para motores de busca"> 
				<img src="<?php echo URL; ?>assets/img/svg/anima-seo/linha.svg" class="wow rollIn" data-wow-delay="0.8s" alt="SEO Otimização para motores de busca"> 
				<img src="<?php echo URL; ?>assets/img/svg/anima-seo/grafico3.svg" class="wow flipInY" data-wow-delay="0.6s" alt="SEO Otimização para motores de busca"> 
				<img src="<?php echo URL; ?>assets/img/svg/anima-seo/telaGrafico.svg" class="wow fadeInUp" data-wow-delay="0.9s" alt="SEO Otimização para motores de busca"> 
				<img src="<?php echo URL; ?>assets/img/svg/anima-seo/lupa.svg" class="wow rotateIn" data-wow-delay="0.6s" alt="SEO Otimização para motores de busca"> 
				<img src="<?php echo URL; ?>assets/img/svg/anima-seo/persona1.svg" class="wow fadeInUp" data-wow-delay="0.6s" alt="SEO Otimização para motores de busca"> 
				<img src="<?php echo URL; ?>assets/img/svg/anima-seo/persona2.svg" class="wow zoomInUp" data-wow-delay="0.6s" alt="SEO Otimização para motores de busca"> 				
				<img src="<?php echo URL; ?>assets/img/svg/anima-seo/pc.svg" class="wow fadeInUp" data-wow-delay="0.9s" alt="SEO Otimização para motores de busca">
				<img src="<?php echo URL; ?>assets/img/svg/anima-sistema-web/hashtag.png" class="wow fadeInDown" data-wow-delay="0.12s" alt="SEO Otimização para motores de busca">
				<img src="<?php echo URL; ?>assets/img/svg/anima-sistema-web/end-web.png" class="wow fadeInDown" data-wow-delay="0.12s" alt="SEO Otimização para motores de busca">
				<img src="<?php echo URL; ?>assets/img/svg/anima-seo/seo.svg" class="wow zoomIn" data-wow-delay="0.6s" alt="SEO Otimização para motores de busca"> 
			  </div>
          </div>
        </div>
        <div class="banner-sistema-web-titulo">SEO</div>
      </div>
    </div>
  </div>
  <div class="shape-img1"><img src="<?php echo URL; ?>assets/img/shape/1.png" alt="SEO Otimização para motores de busca"></div>
  <div class="shape-img2"><img src="<?php echo URL; ?>assets/img/shape/2.png" alt="SEO"></div>
  <div class="shape-img3"><img src="<?php echo URL; ?>assets/img/shape/3.png" alt="SEO"></div>
  <div class="shape-img4"><img src="<?php echo URL; ?>assets/img/shape/4.png" alt="SEO"></div>
  <div class="shape-img5"><img src="<?php echo URL; ?>assets/img/shape/5.png" alt="SEO"></div>
  <div class="shape-img6"><img src="<?php echo URL; ?>assets/img/shape/6.png" alt="SEO"></div>
  <div class="shape-img7"><img src="<?php echo URL; ?>assets/img/shape/2.png" alt="SEO"></div>
  <div class="shape-img8"><img src="<?php echo URL; ?>assets/img/shape/10.png" alt="SEO"></div>
  <div class="shape-img9"><img src="<?php echo URL; ?>assets/img/shape/7.png" alt="SEO"></div>
  <div class="shape-img10"><img src="<?php echo URL; ?>assets/img/shape/5.png" alt="SEO"></div>
  <div class="shape-img11"><img src="<?php echo URL; ?>assets/img/shape/11.png" alt="SEO"></div>
</div>
<!-- End Main Banner Area --> 
<section class="services-details-area ptb-100">
	<div class="container">
		<div class="services-details-overview">
			<div class="row align-items-center">
				<div class="col-lg-6 col-md-12">
					<div class="services-details-desc">
						<h3>Chegue no topo dos rankings do Google</h3>
						<p>SEO (Otimização para motores de busca) é o principal canal chave do Marketing Digital. O objetivo do SEO é alcançar as melhores classificações possíveis para o seu site em motores de busca. SEO alcança este resultado ao melhorar o seu website em duas áreas chave:</p>
						<ul>
							<li>Onpage Optimization: Esta área de SEO centra-se no próprio site, procurando otimizar a forma como os utilizadores e os Motores de Busca interagem com ele. Isto inclui o conteúdo do site e configurações técnicas.</li>
							<li>Otimização Offpage: Este campo de SEO procura otimizar os fatores externos que impactam os rankings do seu site, tais como links de entrada, referências em mídias sociais e listagens de diretórios de negócios. Todas estas áreas são fatores de ranking. Isto significa que melhorá-los irá aumentar a classificação do seu site nos motores de busca.</li>
						</ul>
						<h4>Porque preciso de fazer SEO?</h4>
						<p>Obter classificações de topo na primeira página do Google para os termos que os potenciais clientes usam para encontrar negócios no seu setor, trará mais clientes para o seu site. Ao contrário dos anúncios pagos, que só lhe dão visibilidade enquanto você pagar por eles, as classificações orgânicas não custam um centavo, o que significa mais lucros agora e a longo prazo.</p>
						
						<p>Como você não é o único que luta por esses rankings de topo no Google, é importante entender que você tem concorrência. SEO é um investimento a longo prazo que levará o Marketing Digital do seu negócio para o próximo nível, mas este trabalho demorará algum tempo para ser totalmente recompensado. Dentro de semanas você começará a ver os resultados. Dentro de poucos meses você terá construído uma estratégia eficiente, econômica e de longo prazo para atrair muitos novos clientes.</p>
						
					</div>
				</div>

				<div class="col-lg-6 col-md-12">
					<div class="services-details-image">
						<img src="<?php echo URL; ?>assets/img/svg/seo-supremo-digital.svg" alt="Supremo Digital - SEO">
					</div>
				</div>
			</div>
		</div>
		<div class="services-details-overview">
			<div class="row align-items-center">
				<div class="col-lg-12 col-md-12">
					
					<blockquote class="wp-block-quote">
						<p class="frasePg">Estima-se que o Google utiliza mais de 250 variáveis para determinar o posicionamento de uma página. Esses fatores compõem o algoritmo orgânico, um segredo empresarial tão bem guardando quanto a fórmula da Coca-Cola.</p>

						<cite>Alessandro Palmeira</cite>
					</blockquote>
					
				</div>
			</div>
		</div> 
		<div class="services-details-overview">
			<div class="row align-items-center">
				<div class="col-lg-6 col-md-12">
					<div class="services-details-image">
						<img src="<?php echo URL; ?>assets/img/svg/supremo-digital-metodologia.svg" alt="Supremo Digital Aplicação Web">
					</div>
				</div>

				<div class="col-lg-6 col-md-12">
					<div class="services-details-desc">
						<h3>Um pequeno passo a passo para o sucesso online </h3>
						<p>Maximize a sua visibilidade online e o tráfego orgânico do seu site com SEO (otimização para motores de busca).</p>
						<div class="services-details-accordion">
							<ul class="accordion">
								<li class="accordion-item">
									<a class="accordion-title active" href="javascript:void(0)">
										<i class="fas fa-plus"></i>
										Encontre as melhores palavras-chave:
									</a>

									<p class="accordion-content show">Obter as palavras-chave certas para o seu website é o primeiro passo para alcançar o sucesso com SEO. A Supremo Digital analisa o seu website e a sua concorrência para encontrar as palavras-chave perfeitas para o seu mercado.</p>
								</li>

								<li class="accordion-item">
									<a class="accordion-title" href="javascript:void(0)">
										<i class="fas fa-plus"></i>
										Encontre fontes de backlink
									</a>

									<p class="accordion-content">Os backlinks que os seus concorrentes têm em comum ajuda-o a encontrar e construir os seus próprios backlinks de forma mais eficiente e eficaz.</p>
								</li>

								<li class="accordion-item">
									<a class="accordion-title" href="javascript:void(0)">
										<i class="fas fa-plus"></i>
										Google Analytics 
									</a>

									<p class="accordion-content">Meça o seu sucesso com o Google Analytics - Integre seu site Google Analytics  para ver de onde seus clientes estão vindo e como o número de visitantes está melhorando.</p>
								</li>
								
								<li class="accordion-item">
									<a class="accordion-title" href="javascript:void(0)">
										<i class="fas fa-plus"></i>
										Google Meu Negócio 
									</a>

									<p class="accordion-content">Crie uma conta no Google Meu Negócio e integre as listagens existentes. Defina as suas listagens para sincronizar automaticamente em mais de 30 diretórios e monitorize estas listagens.</p>
								</li>
								
								
								
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section> 