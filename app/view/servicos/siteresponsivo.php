<link rel="stylesheet" href="<?php echo URL; ?>assets/css/banner-site-responsivo.css">
<!-- Start Main Banner Area -->
<div class="main-banner">
  <div class="d-table">
    <div class="d-table-cell">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-lg-6 col-md-12">
            <div class="main-banner-content">
              <h1>SITE RESPONSIVO</h1>
              <p>Sua empresa no ar 24 horas por dia, 7 dias por semana e 365 dias por ano...</p>			
            </div>
          </div>
          <div class="col-lg-6 col-md-12">
            <div class="banner-image banner-sistema-web"> 
				<img src="<?php echo URL; ?>assets/img/svg/anima-sistema-web/fundo-grid.png" class="wow fadeInLeft" data-wow-delay="0.6s" alt="SITE RESPONSIVO"> 
				<img src="<?php echo URL; ?>assets/img/svg/anima-site-responsivo/social1.svg" class="wow fadeInUp" data-wow-delay="0.6s" alt="SITE RESPONSIVO"> 
				<img src="<?php echo URL; ?>assets/img/svg/anima-site-responsivo/pc.svg" class="wow rotateIn" data-wow-delay="0.8s" alt="SITE RESPONSIVO"> 
				<img src="<?php echo URL; ?>assets/img/svg/anima-site-responsivo/ferramenta1.svg" class="wow zoomIn" data-wow-delay="0.7s" alt="SITE RESPONSIVO"> 
				<img src="<?php echo URL; ?>assets/img/svg/anima-site-responsivo/tag.svg" class="wow bounceIn" data-wow-delay="0.7s" alt="SITE RESPONSIVO"> 
				<img src="<?php echo URL; ?>assets/img/svg/anima-site-responsivo/pesquisa.svg" class="wow rollIn" data-wow-delay="0.8s" alt="SITE RESPONSIVO"> 
				<img src="<?php echo URL; ?>assets/img/svg/anima-site-responsivo/celular.svg" class="wow flipInY" data-wow-delay="0.6s" alt="SITE RESPONSIVO"> 
				<img src="<?php echo URL; ?>assets/img/svg/anima-site-responsivo/ferramenta2.svg" class="wow fadeInUp" data-wow-delay="0.9s" alt="SITE RESPONSIVO"> 
				<img src="<?php echo URL; ?>assets/img/svg/anima-site-responsivo/bloco3.svg" class="wow rotateIn" data-wow-delay="0.6s" alt="SITE RESPONSIVO"> 
				<img src="<?php echo URL; ?>assets/img/svg/anima-site-responsivo/bloco2.svg" class="wow fadeInUp" data-wow-delay="0.6s" alt="SITE RESPONSIVO"> 
				<img src="<?php echo URL; ?>assets/img/svg/anima-site-responsivo/bloco1.svg" class="wow zoomInUp" data-wow-delay="0.6s" alt="SITE RESPONSIVO"> 				
				<img src="<?php echo URL; ?>assets/img/svg/anima-sistema-web/end-web.png" class="wow fadeInUp" data-wow-delay="0.9s" alt="SITE RESPONSIVO">
				<img src="<?php echo URL; ?>assets/img/svg/anima-sistema-web/hashtag.png" class="wow fadeInDown" data-wow-delay="0.12s" alt="SITE RESPONSIVO">
				 
				<img src="<?php echo URL; ?>assets/img/svg/anima-site-responsivo/site-responsivo.svg" class="wow zoomIn" data-wow-delay="0.6s" alt="SITE RESPONSIVO"> 
			  </div>
          </div>
        </div>
        <div class="banner-sistema-web-titulo">RESPONSIVO</div>
      </div>
    </div>
  </div>
  <div class="shape-img1"><img src="<?php echo URL; ?>assets/img/shape/1.png" alt="SITE RESPONSIVO"></div>
  <div class="shape-img2"><img src="<?php echo URL; ?>assets/img/shape/2.png" alt="SITE RESPONSIVO"></div>
  <div class="shape-img3"><img src="<?php echo URL; ?>assets/img/shape/3.png" alt="SITE RESPONSIVO"></div>
  <div class="shape-img4"><img src="<?php echo URL; ?>assets/img/shape/4.png" alt="SITE RESPONSIVO"></div>
  <div class="shape-img5"><img src="<?php echo URL; ?>assets/img/shape/5.png" alt="SITE RESPONSIVO"></div>
  <div class="shape-img6"><img src="<?php echo URL; ?>assets/img/shape/6.png" alt="SITE RESPONSIVO"></div>
  <div class="shape-img7"><img src="<?php echo URL; ?>assets/img/shape/2.png" alt="SITE RESPONSIVO"></div>
  <div class="shape-img8"><img src="<?php echo URL; ?>assets/img/shape/10.png" alt="SITE RESPONSIVO"></div>
  <div class="shape-img9"><img src="<?php echo URL; ?>assets/img/shape/7.png" alt="SITE RESPONSIVO"></div>
  <div class="shape-img10"><img src="<?php echo URL; ?>assets/img/shape/5.png" alt="SITE RESPONSIVO"></div>
  <div class="shape-img11"><img src="<?php echo URL; ?>assets/img/shape/11.png" alt="SITE RESPONSIVO"></div>
</div>
<!-- End Main Banner Area --> 
<section class="services-details-area ptb-100">
	<div class="container">
		<div class="services-details-overview">
			<div class="row align-items-center">
				<div class="col-lg-6 col-md-12">
					<div class="services-details-desc">
						<h3>Um Site Completo, Não Precisa Ser Complexo</h3>
						<p>Já dizia o ditado “quem não se adapta está fora”. Ter um site responsivo, que se adapte a telas de computadores, celulares e tablets é fator obrigatório para que sua empresa possa impactar o maior número de usuários e onde eles estiverem.</p>
						
						<p>Soluções completas para o desenvolvimento de sites institucionais, comerciais, pessoais, dinâmicos, promocionais e administráveis, integração com redes sociais com excelência no design, na arquitetura da informação, na usabilidade, na otimização para mecanismo de busca, na compatibilidade com múltiplos navegadores e na acessibilidade por dispositivos móveis, como Iphone, Ipad, Smartphones e Tablets em geral.</p>
						
					</div>
				</div>

				<div class="col-lg-6 col-md-12">
					<div class="services-details-image">
						<img src="<?php echo URL; ?>assets/img/svg/supremo-digital-sobre.svg" alt="Supremo Digital site Responsivo">
					</div>
				</div>
			</div>
		</div>
		<div class="services-details-overview">
			<div class="row align-items-center">
				<div class="col-lg-12 col-md-12">
					
					<blockquote class="wp-block-quote">
						<p class="frasePg">Agora que você já sabe as diferenças entre um Sistema Web e um Site. Já pensou em qual plataforma sua empresa precisa? Já pensou em desenvolver uma aplicação?</p>

						<cite>Alessandro Palmeira</cite>
					</blockquote>
					
				</div>
			</div>
		</div>  
	</div>
</section> 